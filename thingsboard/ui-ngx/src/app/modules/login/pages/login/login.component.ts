///
/// Copyright © 2016-2021 The Thingsboard Authors
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///

import { Component, OnInit, ViewChild, ElementRef ,AfterViewInit} from '@angular/core';
import { AuthService } from '@core/auth/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '@core/core.state';
import { PageComponent } from '@shared/components/page.component';
import { FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Constants } from '@shared/models/constants';
import { Router } from '@angular/router';
import { OAuth2ClientInfo } from '@shared/models/oauth2.models';
import {Html5QrcodeScanner,Html5Qrcode,Html5QrcodeSupportedFormats} from "html5-qrcode"

@Component({
  selector: 'tb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends PageComponent implements OnInit , AfterViewInit {
  @ViewChild('qrreader') qrreader: ElementRef;

  loginFormGroup = this.fb.group({
    username: '',
    password: ''
  });
  oauth2Clients: Array<OAuth2ClientInfo> = null;
  hidePassword = true;
  html5QrcodeScanner = null;
  html5QrCode = null;
  
  constructor(protected store: Store<AppState>,
              private authService: AuthService,
              public fb: FormBuilder,
              private router: Router) {
    super(store);
  }

  onScanSuccess(decodedText, decodedResult) {
    // Handle on success condition with the decoded text or result.
    console.log(`Scan result: ${decodedText}`);
    // ^ this will stop the scanner (video feed) and clear the scan area.
    //this.html5QrcodeScanner.clear();
    if(decodedText.includes("hawkeye")){
      // found login credentials qr code
      var splitText = decodedText.split(":");
      if (splitText.length == 2){
        var credentialsText =  splitText[1];
        var credentials = credentialsText.split(",");
        this.html5QrCode.stop();
        this.loginFormGroup.get('username').patchValue(credentials[0]);
        this.loginFormGroup.get('password').patchValue(credentials[1]);
        //this.loginFormGroup.get('username').patchValue('tenant@msense.ai');
        //this.loginFormGroup.get('password').patchValue('msensetenant@123');
        this.login(); //login
      }
    }
  }

  onScanError(errorMessage) {
    // handle on error condition, with error message
    console.log(`Scan Error: ${errorMessage}`);
  }
  ngOnInit() {
    this.oauth2Clients = this.authService.oauth2Clients;
  }

  ngAfterViewInit() {

    /*this.html5QrcodeScanner = new Html5QrcodeScanner(
      'qrreader',
      { fps: 10, qrbox: {width: 250, height: 250} },
      false);
    this.html5QrcodeScanner.render(this.onScanSuccess.bind(this));*/  
    //this.html5QrcodeScanner.render(this.onScanSuccess.bind(this), this.onScanError.bind(this));
    this.html5QrCode = new Html5Qrcode("qrreader");
    //const config = { fps: 10, qrbox: { width: 250, height: 250 }};
    // If you want to prefer back camera
    //this.html5QrCode.start({ facingMode: "environment" }, config, this.onScanSuccess.bind(this));
  }

  startScan(): void {

    const config = { fps: 10, qrbox: { width: 250, height: 250 } };
    console.log("inside startscan");
    // If you want to prefer back camera
    this.html5QrCode.start({ facingMode: "environment" }, config, this.onScanSuccess.bind(this));
    console.log("exiting startscan");
  }

  login(): void {
    if (this.loginFormGroup.valid) {
      this.authService.login(this.loginFormGroup.value).subscribe(
        () => {},
        (error: HttpErrorResponse) => {
          if (error && error.error && error.error.errorCode) {
            if (error.error.errorCode === Constants.serverErrorCode.credentialsExpired) {
              this.router.navigateByUrl(`login/resetExpiredPassword?resetToken=${error.error.resetToken}`);
            }
          }
        }
      );
    } else {
      Object.keys(this.loginFormGroup.controls).forEach(field => {
        const control = this.loginFormGroup.get(field);
        control.markAsTouched({onlySelf: true});
      });
    }
  }

}
