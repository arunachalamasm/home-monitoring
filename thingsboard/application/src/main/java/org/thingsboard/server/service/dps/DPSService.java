// Copyright 2021, mSense Inc
// All rights reserved.

package org.thingsboard.server.service.dps;

import com.fasterxml.jackson.databind.JsonNode;
public interface DPSService {

    JsonNode deviceProvision(JsonNode value);
    
}
