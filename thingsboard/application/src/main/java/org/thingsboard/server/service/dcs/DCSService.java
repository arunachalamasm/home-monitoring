// Copyright 2021, mSense Inc
// All rights reserved.

package org.thingsboard.server.service.dcs;

import com.fasterxml.jackson.databind.JsonNode;
public interface DCSService {

    JsonNode deviceCreation(JsonNode value);
    
}
