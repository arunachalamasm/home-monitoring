// Copyright 2021, mSense Inc
// All rights reserved.

package org.thingsboard.server.service.ucs;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
public interface UCSService {

    JsonNode userCreation(JsonNode value, HttpServletRequest httpServletRequest);
    
}