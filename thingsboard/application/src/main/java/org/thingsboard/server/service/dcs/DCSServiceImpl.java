// Copyright 2021, mSense Inc
// All rights reserved.

package org.thingsboard.server.service.dcs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thingsboard.server.common.data.AdminSettings;
import org.thingsboard.server.common.data.Customer;
import org.thingsboard.server.common.data.DataConstants;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.DeviceProfile;
import org.thingsboard.server.common.data.DeviceProfileProvisionType;
import org.thingsboard.server.common.data.DeviceProfileType;
import org.thingsboard.server.common.data.DeviceTransportType;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.TenantProfile;
import org.thingsboard.server.common.data.User;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.UserId;
import org.thingsboard.server.common.data.id.DeviceProfileId;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.PageLink;
import org.thingsboard.server.common.data.query.KeyFilter;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.common.data.security.DeviceCredentials;
import org.thingsboard.server.common.data.security.UserCredentials;
import org.thingsboard.server.common.data.tenant.profile.TenantProfileData;
import org.thingsboard.server.dao.customer.CustomerService;
import org.thingsboard.server.dao.device.DeviceCredentialsService;
import org.thingsboard.server.dao.device.DeviceProfileService;
import org.thingsboard.server.dao.device.DeviceService;
import org.thingsboard.server.dao.tenant.TenantProfileService;
import org.thingsboard.server.dao.tenant.TenantService;
import org.thingsboard.server.dao.user.UserService;
import org.thingsboard.server.queue.util.TbCoreComponent;
import org.thingsboard.server.common.data.page.PageData;
import org.thingsboard.server.common.data.id.DashboardId;
import org.thingsboard.server.common.data.DashboardInfo;
import org.thingsboard.server.common.data.id.IdBased;
import org.thingsboard.server.dao.dashboard.DashboardService;
import org.thingsboard.server.utils.MiscUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;

@Service
@Slf4j
// @TbCoreComponent
public class DCSServiceImpl implements DCSService {

    public static final String DEFAULT_DEVICE_TYPE = "hawkeye";
    //private static final String PUBLIC_CUSTOMER_NAME = "Public";
    private static final ObjectMapper objectMapper = new ObjectMapper();
    

    private TenantId tenantId;


    private CustomerId customerId;

    private UserId userId;

    private DeviceId deviceId;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private DeviceCredentialsService deviceCredentialsService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    // @Bean
    // protected BCryptPasswordEncoder passwordEncoderPass() {
    // return new BCryptPasswordEncoder();
    // }

    public DCSServiceImpl() {
    }

    @Override
    public JsonNode deviceCreation(JsonNode jsonNode) {

        ObjectNode resultNode = objectMapper.createObjectNode();
        resultNode.put("message", MiscUtils.SUCCESS);

        String command = jsonNode.get("command").asText();
        String deviceName = jsonNode.at("/parameters/deviceName").asText();
        String tenantName = jsonNode.at("/parameters/tenantName").asText();
        String deviceType = jsonNode.at("/parameters/deviceType").asText();
        String emailId = jsonNode.at("/parameters/emailId").asText();

        if (StringUtils.isEmpty(command) || StringUtils.isEmpty(deviceName) || StringUtils.isEmpty(tenantName)  || StringUtils.isEmpty(deviceType) || StringUtils.isEmpty(emailId)) {
            return resultNode.put("message", "Bad Request");
        }

        PageLink pageLink = new PageLink(1, 0, tenantName);
        List<Tenant> tenants = tenantService.findTenants(pageLink).getData();
        for (Tenant tenant : tenants) {
            if ((tenant.getName()).equals(tenantName)) {
                tenantId = tenant.getId();
            } else {
                return resultNode.put("message", "TenantName " + tenantName + " not Found");
            }
        }

        Optional<Customer> customerOpt = customerService.findCustomerByTenantIdAndTitle(tenantId, emailId);
        if (customerOpt.isPresent()){
            
            customerId = customerOpt.get().getId();
            System.out.println("Customer id: "+customerId);
        }

        Device device = deviceService.findDeviceByTenantIdAndName(tenantId, deviceName);
        if (device != null && command.equals("add")) {
            return resultNode.put("message", MiscUtils.DEVICE_EXISTS);
        } else if (device != null && command.equals("delete")) {
            deviceService.deleteDevice(tenantId, device.getId());
        } else if (device == null && command.equals("add")) {
            Device deviceBT = createDevice(tenantId, customerId, deviceName, deviceType, deviceName,
                    "Demo device dashboard");
            deviceId = deviceBT.getId();
        } else if (device == null && command.equals("delete")) {
            resultNode.put("message", MiscUtils.DEVICE_NOT_FOUND);
        } else {
            return resultNode.put("message", "Command " + command + " is not Found.");
        }

        return resultNode;
    }

    private Device createDevice(TenantId tenantId, CustomerId customerId, String name, String type, String accessToken,
            String description) {
        Device device = new Device();
        device.setTenantId(tenantId);
        device.setType(type);
        device.setCustomerId(customerId);
        device.setName(name);
        ObjectNode additionalInfo = objectMapper.createObjectNode();
        if (description != null) {
            additionalInfo.put("description", description);
        }
        additionalInfo.put("gateway", true);
        device.setAdditionalInfo(additionalInfo);
        device = deviceService.saveDevice(device);
        DeviceCredentials deviceCredentials = deviceCredentialsService.findDeviceCredentialsByDeviceId(tenantId,
                device.getId());
        deviceCredentials.setCredentialsId(accessToken);
        deviceCredentialsService.updateDeviceCredentials(tenantId, deviceCredentials);
        return device;
    }


}
