// Copyright 2021, mSense Inc
// All rights reserved.

package org.thingsboard.server.service.ucs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thingsboard.server.common.data.AdminSettings;
import org.thingsboard.server.common.data.Customer;
import org.thingsboard.server.common.data.DataConstants;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.DeviceProfile;
import org.thingsboard.server.common.data.DeviceProfileProvisionType;
import org.thingsboard.server.common.data.DeviceProfileType;
import org.thingsboard.server.common.data.DeviceTransportType;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.TenantProfile;
import org.thingsboard.server.common.data.User;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.UserId;
import org.thingsboard.server.common.data.id.DeviceProfileId;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.PageLink;
import org.thingsboard.server.common.data.query.KeyFilter;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.common.data.security.DeviceCredentials;
import org.thingsboard.server.common.data.security.UserCredentials;
import org.thingsboard.server.common.data.tenant.profile.TenantProfileData;
import org.thingsboard.server.dao.customer.CustomerService;
import org.thingsboard.server.dao.device.DeviceCredentialsService;
import org.thingsboard.server.dao.device.DeviceProfileService;
import org.thingsboard.server.dao.device.DeviceService;
import org.thingsboard.server.dao.tenant.TenantProfileService;
import org.thingsboard.server.dao.tenant.TenantService;
import org.thingsboard.server.dao.user.UserService;
import org.thingsboard.server.queue.util.TbCoreComponent;
import org.thingsboard.server.common.data.page.PageData;
import org.thingsboard.server.common.data.id.DashboardId;
import org.thingsboard.server.common.data.DashboardInfo;
import org.thingsboard.server.common.data.id.IdBased;
import org.thingsboard.server.dao.dashboard.DashboardService;
import org.thingsboard.server.service.security.system.SystemSecurityService;
import org.thingsboard.server.utils.MiscUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

@Service
@Slf4j
// @TbCoreComponent
public class UCSServiceImpl implements UCSService {

    public static final String DEFAULT_DEVICE_TYPE = "hawkeye";
    public static final String ACTIVATE_URL_PATTERN = "%s/api/noauth/activate?activateToken=%s";

    private static final ObjectMapper objectMapper = new ObjectMapper();
    

    private TenantId tenantId;

    private DashboardId dashboardId;

    private CustomerId customerId;

    private UserId userId;

    private DeviceId deviceId;

    @Autowired
    private TenantService tenantService;

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private DeviceCredentialsService deviceCredentialsService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private SystemSecurityService systemSecurityService;

    public UCSServiceImpl() {
    }

    @Override
    public JsonNode userCreation(JsonNode jsonNode, HttpServletRequest httpServletRequest) {
        
        String activationUrl = "";

        ObjectNode resultNode = objectMapper.createObjectNode();
        resultNode.put("message", MiscUtils.SUCCESS);
        

        String command = jsonNode.get("command").asText();
        String tenantName = jsonNode.at("/parameters/tenantName").asText();    
        String dashboardName = jsonNode.at("/parameters/dashboardName").asText();
        String emailId = jsonNode.at("/parameters/emailId").asText();
        String customerName = emailId;

        if (StringUtils.isEmpty(command) || command == null || StringUtils.isEmpty(tenantName) || tenantName == null || StringUtils.isEmpty(emailId) || emailId == null ) {
            return resultNode.put("message", "Bad Request");
        }

        PageLink pageLink = new PageLink(1, 0, tenantName);
        List<Tenant> tenants = tenantService.findTenants(pageLink).getData();
        for (Tenant tenant : tenants) {
            if ((tenant.getName()).equals(tenantName)) {
                tenantId = tenant.getId();
            } else {
                return resultNode.put("message", "TenantName " + tenantName + " not Found");
            }
        }

        PageLink searchTextLink = new PageLink(1, 0, dashboardName);
        List<DashboardInfo> dashboardsPage = dashboardService.findDashboardsByTenantId(tenantId, searchTextLink)
                .getData();
        for (DashboardInfo dashboard : dashboardsPage) {
            if ((dashboard.getName()).equals(dashboardName)) {
                dashboardId = dashboard.getId();
            } else {
                return resultNode.put("message", "DashboardName " + dashboardName + " not Found");
            }
        }

        Optional<Customer> customerOpt = customerService.findCustomerByTenantIdAndTitle(tenantId, customerName);
        if (customerOpt.isPresent() && command.equals("add")) {
            User user = userService.findUserByEmail(tenantId, customerName);
            activationUrl = getActivationLink(tenantId,customerId,user.getId(), httpServletRequest);
            resultNode.put("activationUrl",activationUrl);
            return resultNode.put("message", MiscUtils.USER_EXISTS);
        } else if (customerOpt.isPresent() && command.equals("delete")) {
            customerService.deleteCustomer(tenantId, customerOpt.get().getId());
            return resultNode.put("message", MiscUtils.SUCCESS);
        } else if (!customerOpt.isPresent() && command.equals("add")) {
            Customer customerBT = new Customer();
            customerBT.setTenantId(tenantId);
            customerBT.setTitle(customerName);
            customerBT = customerService.saveCustomer(customerBT);
            customerId = customerBT.getId();
        } else if (!customerOpt.isPresent() && command.equals("delete")) {
            resultNode.put("message", MiscUtils.USER_NOT_FOUND);
        } else {
            return resultNode.put("message", "Command " + command + " is not Found.");
        }

        User user = userService.findUserByEmail(tenantId, customerName);
        if (user != null && command.equals("add")) {
            return resultNode.put("message", MiscUtils.USER_EXISTS);
        } else if (user == null && command.equals("add")) {
            User userBT = createUser(Authority.CUSTOMER_USER, tenantId, customerId, emailId, customerName,
                    dashboardId);
            userId = userBT.getId();
        } else {
            return resultNode.put("message",  "Command " + command + " is not Found.");
        }



        DashboardInfo dashboardInfo = dashboardService.findDashboardInfoById(tenantId, dashboardId);
        if (dashboardInfo == null && command.equals("add")) {
            return resultNode.put("message", dashboardName + "Dashboard is not Found");
        } else if (dashboardInfo != null && command.equals("add")) {
            dashboardService.assignDashboardToCustomer(tenantId, dashboardId, customerId);
        }
        else {
            return resultNode.put("message", "Command " + command + " is not Found.");
        }


        activationUrl = getActivationLink(tenantId,customerId,userId, httpServletRequest);
        resultNode.put("activationUrl",activationUrl);
        
        return resultNode;

        
    }

    

    private User createUser(Authority authority, TenantId tenantId, CustomerId customerId, String email,
            String password, DashboardId dashboardId) {
        User user = new User();
        user.setAuthority(authority);
        user.setEmail(email);
        user.setTenantId(tenantId);
        user.setCustomerId(customerId);
        ObjectNode additionalInfo = objectMapper.createObjectNode();
        if (dashboardId != null) {
            additionalInfo.put("defaultDashboardId", dashboardId.toString());
        }
        additionalInfo.put("defaultDashboardFullscreen", true);
        user.setAdditionalInfo(additionalInfo);
        user = userService.saveUser(user);
        return user;

    }



   private String getActivationLink(TenantId tenantId,CustomerId customerId, UserId userId, HttpServletRequest httpServletRequest){    
    String activationUrl = "";
    String baseUrl = systemSecurityService.getBaseUrl(tenantId, customerId, httpServletRequest);
    UserCredentials userCredentials = userService.findUserCredentialsByUserId(tenantId, userId);
    
    if (!userCredentials.isEnabled() && userCredentials.getActivateToken() != null) {
    
        activationUrl = String.format(ACTIVATE_URL_PATTERN, baseUrl, userCredentials.getActivateToken());
    
    } else {
        activationUrl = baseUrl; 
    }            
        return activationUrl;
    }
}
