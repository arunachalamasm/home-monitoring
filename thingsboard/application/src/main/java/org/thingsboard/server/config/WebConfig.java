/**
 * Copyright © 2016-2021 The Thingsboard Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.config;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thingsboard.server.service.dcs.DCSService;
import org.thingsboard.server.service.ucs.UCSService;
import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.databind.JsonNode;
import org.thingsboard.server.utils.MiscUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import javax.servlet.http.HttpServletRequest;

@Controller
@EnableAutoConfiguration
public class WebConfig {

    @Value("${upload_file.location:}")
    private String uploadFileLocation; // upload file location
 
    @Autowired
    protected DCSService dcsService;
    
    @Autowired
    protected UCSService ucsService;

    @RequestMapping(value = "/{path:^(?!api$)(?!assets$)(?!static$)(?!webjars$)[^\\.]*}/**")
    public String redirect() {
        return "forward:/index.html";
    }

    @PostMapping("/dcs")
    @ResponseBody
    public ResponseEntity<JsonNode> dcs(@RequestBody JsonNode request){
        
        JsonNode jsonNode = dcsService.deviceCreation(request);

        if (jsonNode.get("message").asText().equals(MiscUtils.SUCCESS)) {
            return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.OK);
            
        }
        else if (jsonNode.get("message").asText().equals(MiscUtils.DEVICE_EXISTS)) {
            return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.CONFLICT);
        }

        else if (jsonNode.get("message").asText().equals(MiscUtils.DEVICE_NOT_FOUND)) {
            return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.BAD_REQUEST);
        
    }


    @PostMapping("/ucs")
    @ResponseBody
    public ResponseEntity<JsonNode> ucs(@RequestBody JsonNode request, HttpServletRequest httpServletRequest){
        
        JsonNode jsonNode = ucsService.userCreation(request, httpServletRequest);

        if (jsonNode.get("message").asText().equals(MiscUtils.SUCCESS)) {
            return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.OK);
            
        }
        else if (jsonNode.get("message").asText().equals(MiscUtils.USER_EXISTS)) {
            return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.CONFLICT);
        }

        else if (jsonNode.get("message").asText().equals(MiscUtils.USER_NOT_FOUND)) {
            return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<JsonNode>(jsonNode, HttpStatus.BAD_REQUEST);
        
    }

    @RequestMapping(value = "/files/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Path uploadRootPath = Paths.get(uploadFileLocation);
        try {
            Path file = uploadRootPath.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);

            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @PostMapping("/uploadFile")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {

        if (multipartFile == null || multipartFile.isEmpty()) {
                        String response = "{\"message\":\"Upload file is empty\", \"status\":1}"; 
                        return new ResponseEntity<String>(response, HttpStatus.CREATED);
                        
        }
        String fileName = multipartFile.getOriginalFilename();
        Path uploadPath = Paths.get(uploadFileLocation);
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        Path filePath;
        try (InputStream inputStream = multipartFile.getInputStream()) {
            filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {        
            throw new IOException("Could not save image file: " + fileName, ioe);
        }   
        String fileUrl = "/files/" + fileName;     
        String response = "{\"url\":\"" + fileUrl + "\", \"status\":0}"; 
        return new ResponseEntity<String>(response, HttpStatus.CREATED);
    }
}
