#
#
import sys
import time
import paho.mqtt.client as mqtt
from  hawkeye_logging import get_logger
logger = get_logger(logger_name = __name__, use_color=False)

g_subscribe_topic = None

def on_local_publish(client, userdata, mid):
    #print("Published message to local broker. mid: {0}".format(str(mid)))
    pass

def on_local_connect(client, userdata, flags, rc):
    global g_subscribe_topic
    if (rc >1):
        logger.error("Connection refused with rc: {}".format(str(rc)))
        sys.exit(rc)
    else:
         logger.debug("Connection established to mqtt broker at {}:{}".format(client._host, client._port))
         client.connected_flag=True
         # subscribe to message on reconnect
         if(g_subscribe_topic !=None):
             client.subscribe(topic=g_subscribe_topic)


def on_local_disconnect(client, userdata, rc):
    #"""Callback for when a device disconnects."""
    pass
    # if rc != 0:
    #     print( "Unexpected disconnection with mqtt broker. rc: " + str(rc) )
    #     print( "Reconnecting.. " )
    #     client.reconnect() 
    #     print( "Reconnected " )

    # else:
    #     pass
    #     #print( "Normal disconnect with rc: " + str(rc) + "! Datetime: " + str(datetime.datetime.now()) )


# Keep Alive - This interval defines the maximum length of time that the broker and client may not communicate with each other.
# If the Keep Alive value is non-zero and the Server does not receive a Control Packet from the
# Client 538 within one and a half times the Keep Alive time period, it MUST disconnect the 
# Network Connection to the 539 Client as if the network had failed [MQTT-3.1.2-24].
def mqtt_local(broker_host="127.0.0.1",broker_port=1883, clientid="", on_msg_callback=None, subscribe_topic= None, access_token = None):
    global g_subscribe_topic
    
    mqtt.Client.connected_flag = False  # define connected flag as part of mqtt client object
    mqttc = mqtt.Client(client_id=clientid, )
    if access_token is not None:
        mqttc.username_pw_set(username=access_token, password=None)
    mqttc.on_connect = on_local_connect
    mqttc.on_publish = on_local_publish
    mqttc.on_disconnect = on_local_disconnect
    g_subscribe_topic = subscribe_topic
    if(on_msg_callback != None):
        mqttc.on_message = on_msg_callback
    mqttc.loop_start()  # threaded interface to the network loop. also handles reconnecting to the broker.
    #mqttc.on_log=on_log
    try:
        mqttc.connect(host=broker_host, port=broker_port, keepalive=60, bind_address="")
    except Exception as e:
        logger.error("MQTT local connection failed: {} ".format(str(e)))

    while not mqttc.connected_flag: #wait in loop
        logger.debug("In Connection wait loop")
        time.sleep(2)

    
    logger.debug("Connection Successful!!")

    return mqttc

# def main():
#     mqtt_local(clientid="hawkeye_device")

# if __name__ == '__main__':
#     main()
#     #sys.exit(0)
 
