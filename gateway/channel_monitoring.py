"ver:1.1"
"""Scan for targets and and pretty print some data."""
"added support to kill wlanOmon if application restarts"
import asyncio

import pyrcrack

from rich.console import Console
from rich.prompt import Prompt
import traceback
import logging
import json
import os
import hawk_util as hawk_util
from hawk_util import *
logging.basicConfig(level=logging.DEBUG)
import subprocess
import time
from  hawkeye_logging import get_logger
logger = get_logger(logger_name = __name__, use_color=False)
ch = ""
bssid_update = ""

async def scan_for_targets(wireless_network, wireless_interface, json_data):
    global ch , bssid_update
    """Scan for targets, return json."""
    #console = Console()
    #console.clear()
    #console.show_cursor(False)
    airmon = pyrcrack.AirmonNg()
    interfaces = await airmon.interfaces
    logger.debug("running")
    l = [interfaces[i].interface for i in range(len(interfaces))]
    if "wlan0mon" in l:
        try:
            #z = ['sudo airmon-ng check kill','sudo systemctl start NetworkManager','sudo airmon-ng stop wlan0','sudo airmon-ng stop wlan0mon']
            z = ['sudo airmon-ng stop wlan0mon']
            for cmd in z:
                logger.info("running cmd:{}".format(cmd))
                res = subprocess.check_output(cmd,shell=True)
                time.sleep(2)
        except:
            logger.error("command start failed!!")
    logger.info(l)
    interface = wireless_interface

    async with airmon(interface) as mon:
        async with pyrcrack.AirodumpNg() as pdump:
            async for aps in pdump(mon.monitor_interface):
                #console.clear()
                #console.print(aps.table)
                l = [aps[i].essid for i in range(len(aps))]
                try:
                    client = l.index(wireless_network)
                    logger.debug(client)
                except ValueError:
                    client = "continue"
                    # continue
                if client != 'continue':
                    break

        async with pyrcrack.AirodumpNg() as pdump:

            async for result in pdump(mon.monitor_interface,
                                      **aps[int(client)].airodump):
                #console.clear()
                #console.print(result.table)
                l = [result[i].channel for i in range(len(result))]
                bssid = [result[i].bssid for i in range(len(result))] # bssid
                if bssid[0] != bssid_update:
                    with open("configuration.json","w") as fb:
                        json_data["bssid"] = bssid[0]
                        fb.write(json.dumps(json_data))
                    logger.debug("updating BSSID")
                    bssid_update = bssid[0]

                logger.debug(l)
                   
                if l[0] != ch:
                    ch = l[0]
                await asyncio.sleep(3)


def parse_inputconf():
    '''
    parse inputs from JSON
    :return:
    '''
    try:
        with open(CONFIG_FILE, "r") as fb:
            json_data = json.loads(fb.read())
        wireless_network = json_data["wpa_credentials"][0].get("ssid")
        wireless_interface = json_data["wireless_interface"]
        password = json_data["wpa_credentials"][0].get("password")
        return wireless_network,wireless_interface,json_data
    except:
        logger.error("failed to parse input JSON")
        traceback.print_exc()

if __name__ == "__main__":
#check for device provisoned 
    is_provisoned = False

    while is_provisoned is False:
        logger.debug("WiFi Configuration is not found.")
        DPS_TRACKER_FILE = DPS_TRACKER_DIR + CLIENT_CERT
        CONFIG_TRACKER_FILE = src_dir +"/"+ CONFIG_FILE 
        if os.path.exists(DPS_TRACKER_FILE) and os.path.exists(CONFIG_TRACKER_FILE):
            is_provisoned = True
        else:
            is_provisoned = False
        time.sleep(2)        


    wireless_network,wireless_interface,json_data = parse_inputconf()

    asyncio.run(scan_for_targets(wireless_network,wireless_interface,json_data))
