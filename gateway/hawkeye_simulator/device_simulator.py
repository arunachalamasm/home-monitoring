import paho.mqtt.client as mqtt
from time import sleep
import random
import csv
import json
# broker="test.mosquitto.org"


host_name = '127.0.0.1'
port = 1883
qos = 1
topic_pub = '/hawkeye/data'

siteVisited = ["www.google.com", "www.msense.ai", "www.facebook.com", "www.twitter.com", "www.instagram.com", "www.gmail.com", "www.toi.com", "www.thehindu.in", "www.telegragh.com", "www.youtube.com", "www.oracle.com", "www.india.gov.in", "www.wikipedia.org", "www.mygov.in", "www.covid19.org"]

client = mqtt.Client()

def publish_msg(payload_data):
    client.connect(host_name, port, qos)
    client.publish(topic_pub, payload_data)
    # client.disconnect()

i = 0
while(i < 10000000000):
    with open('device_data.csv') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            x = random.randrange(0, 14)
            payload = {}
            payload['type'] = row[0]
            payload['isActive'] = True if row[1] == "true" else False
            payload['hostName'] = row[2]
            payload['serialNumber'] = row[3]
            payload['network'] = row[4]
            payload['siteVisited'] = siteVisited[x]
            payload['deviceType'] = 'hawkeye-node'
            payload_data = json.dumps(payload)
            print("payload_data:", payload_data)
            publish_msg(payload_data)
            sleep(x)
    i = i + 1        
