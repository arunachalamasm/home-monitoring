# Installation of packages on Hawkeye Device
1) ./user_creation.sh
2) reboot
3) Use hawkeye and hawkeye@123 to login
4) ./install.sh

#HawkEye device data packet:

The data packet is in JSON format, key is always a string and is basically a data point key name, while the value can be either string, boolean, double, integer.

Sample Hawkeye monitoring data packet:

	{
		"type": "mobile", 
		"isActive": true, 
		"hostName": "Nihar-mobile01", 
		"serialNumber": "ac:5f:ea:e9:fe:12", 
		"network": "5G", 
		"siteVisited": "www.telegragh.com", 
		"deviceType": "hawkeye-node"
	}

The data packet needs to be sent to local MQTT broker on topic "/hawkeye/data", which is running on port 1883.

The platform supports two types of data points,
1. Attributes Data:
	static data, whose value will not change frequently. For example serialNumber, hostName, firmware version
2. Timeseries Data:
	Dynamic data, which value will change with time, For example siteVisited

	
For mapping hawkeye monitoring packet data with platform data type, we need to configure in hawkeye_mqtt.json connector file.

hawkeye_mqtt.json connector is located at "device_provisioning/gateway" directory.

Sample for mapping hawkeye monitoring packet data(mentioned) with platorm data type,

"mapping": [
    {
      "topicFilter": "/hawkeye/data",
      "converter": {
        "type": "json",
        "deviceNameJsonExpression": "${serialNumber}",
        "deviceTypeJsonExpression": "${deviceType}",
        "timeout": 60000,
        "attributes": [
          {
            "type": "string",
            "key": "type",
            "value": "${type}"
          },
          {
            "type": "boolean",
            "key": "active",
            "value": "${isActive}"
          },
          {
            "type": "string",
            "key": "hostName",
            "value": "${hostName}"
          },
          {
            "type": "string",
            "key": "macAddress",
            "value": "${serialNumber}"
          },
          {
            "type": "string",
            "key": "network",
            "value": "${network}"
          }
        ],
        "timeseries": [
          {
            "type": "string",
            "key": "siteVisited",
            "value": "${siteVisited}"
          }
        ]
      }
    }
  ],


In above json,

topicFilter - MQTT topic on which monitoring library will publish data to local MQTT broker running at port 1883.
deviceNameJsonExpression - mapping for device name. New device will be created on platform if it does not exist.
deviceTypeJsonExpression - mapping for device type to be created on platfrom. 
attributes - this section is mapping of attributes type data, For example we have mapping type, isActive, hostName, serialNumber, network.
timeseries - this section is mapping of timeseries type data.


If you want to add/remove any data point from hawkeye monitoring data packet, modify the mapping for attibutes or timeseries data accordingly in hawkeye_mqtt.json connector file.
