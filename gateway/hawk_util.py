import socket
import netifaces
import json
import os

DEFAULT_MAC_ADDRESS="00:00:00:00:00:00"

#home_dir = str(Path.home())
src_dir = os.getcwd()
DPS_TRACKER_DIR = src_dir + '/.msense_cert/'
 
CONFIG_FILE = "configuration.json"
CA_CERT = "mqttserver.pub.pem" #Server public key
PUBLIC_KEY = "public_key.pem"  
PRIVATE_KEY = "private_key.pem"
CLIENT_CERT = "mqttclient.nopass.pem" #required for gateway ssl configuration

##------------------------------------------------------
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        ip_addr = s.getsockname()[0]
    except Exception as e:
        #logger.error("get_ip() - socket connection error. " + str(e))
        ip_addr = '127.0.0.1'
    finally:
        s.close()
    return ip_addr

#get mac address
def get_mac_address():
    
    result = ""

    network_card_list = netifaces.interfaces()
    for network_card in network_card_list:
        if "lo" not in network_card and "wl" not in network_card: #for filtering ethernet mac address
            network_info = netifaces.ifaddresses(network_card)
            json_mac_address = json.dumps(network_info[netifaces.AF_LINK])
            py_obj = json.loads(json_mac_address) # json-string to python object

            #check if the addr value othern then "00:00:00:00:00:00" (DEFAULT_MAC_ADDRESS)
            if py_obj[0]['addr'] != DEFAULT_MAC_ADDRESS:
                result = py_obj[0]['addr']
                break
       
    return result    