
import re
from xml.dom.minidom import TypeInfo
import netifaces

import traceback
import sys

import time
import os
import paho.mqtt.client as mqtt
import time
import sys
import json
import os
import hawk_mqtt as hawk_mqtt
from hawk_mqtt import *
import hawk_util as hawk_util
from hawk_util import *
import ipaddress
from pathlib import Path
import netifaces
import socket
from  hawkeye_logging import get_logger 

CONFIG_FILE = "./configuration.json"
 
g_mqttc_rpc_processor = None

host_name = '127.0.0.1'
port = 1883
qos = 1
topic_pub = '/hawkeye/rpc'
TOPIC_RPC_CMD = "rpc/+/request/+/+"
TOPIC_RPC_CMD_RESPONCE = "rpc/+/response/+/+"
REBOOT_CMD = "sudo reboot"

HOSTNAME = socket.gethostname()   
IPAddr = socket.gethostbyname(HOSTNAME)
logger = get_logger(logger_name = __name__, use_color=False)

def publish_msg(mqttc,topic, payload):
    payload = json.dumps(payload)     # python object --> json string
    try:
        rc, mid = mqttc.publish(topic=topic, payload=payload, qos=0, retain=False)
        if rc != mqtt.MQTT_ERR_SUCCESS:
            logger.error("publish error. {}".format(mqtt.error_string(rc)))
        else:
            logger.info("published on topic '{}' msg '{}'  with rc {}".format(topic, str(payload), str(rc) ))
            pass

    except Exception as e:
        logger.error("publish error e={}".format(e))
#-------------------------------------------------------------------

def read_wifi_credentials_configuration():
    #file2open = "./configuration.json"
    if not Path(CONFIG_FILE).is_file():
        logger.error("Error: file {} does not exist".format(CONFIG_FILE))
        return None
    with open(CONFIG_FILE) as json_data_file:
        return json.load(json_data_file)


#-----------------------------------------------------------
def write_wifi_credentials_configuration(data):
    #file2open = "./configuration.json"
    if Path(CONFIG_FILE).is_file():
        # logger.debug("Error: file {} does not exist".format(file2open))
        # return None
        with open(CONFIG_FILE,"w") as json_data_file:
            json.dump(data,json_data_file,indent = 4)
    else:
        # Serializing json 
        json_object = json.dumps(data, indent = 4)
        # Writing to json file
        with open(CONFIG_FILE, "w") as json_data_file:
            json_data_file.write(json_object)
    logger.info("Wifi Configuration Successfull.")

#-------------------------------------------------------------
def on_rpc_message(client, userdata, message):
    global g_mqttc_rpc_processor
    # check if this is a retained message, if so return without processing to avoid false reporting
    if message.retain == True:
        logger.info("Received Retained Message")
        return
    else:
        pass

    response = None
    response_topic = message.topic.replace('request', 'response')
    
    rpc_payload_bytes = message.payload
    rpc_payload_str = rpc_payload_bytes.decode("utf-8", errors="ignore")
    logger.debug(rpc_payload_str)
    rpc_payload = json.loads(rpc_payload_str)   # json-string to python object
    
    if Path(CONFIG_FILE).is_file():
        wifi_credentials_config_as_dict = read_wifi_credentials_configuration()
        wifi_credentials = wifi_credentials_config_as_dict.get("wpa_credentials",[])
        for config_dict in wifi_credentials:
            if config_dict.get("ssid") == rpc_payload.get('wifiConfiguration').get("ssid"):
                wifi_credentials.remove(config_dict)
        wifi_credentials.insert(0,rpc_payload.get('wifiConfiguration'))
        wifi_credentials_config_as_dict["wpa_credentials"] = wifi_credentials
        wifi_credentials_config_as_dict["notification_email"] = rpc_payload.get('notification_email')
    else:
        wifi_credentials_config_as_dict = {}
        wifi_credentials_config_as_dict["version"] = "1.0.0"
        wifi_credentials_config_as_dict["wpa_credentials"] = [rpc_payload.get('wifiConfiguration')]
        wifi_credentials_config_as_dict["wireless_interface"] = "wlan0"
        wifi_credentials_config_as_dict["bssid"] = ""
        wifi_credentials_config_as_dict["notification_email"] = rpc_payload.get('notification_email')

    write_wifi_credentials_configuration(wifi_credentials_config_as_dict)
    
    logger.info("Device Reboot..")
    os.system(REBOOT_CMD)                                                                


def main():
    global message_counter
    global g_mqttc_rpc_processor
    #check for device provisoned 
    is_provisoned = False
    while is_provisoned is False:
        DPS_TRACKER_FILE = DPS_TRACKER_DIR + CLIENT_CERT
        if os.path.exists(DPS_TRACKER_FILE):
            is_provisoned = True
        else:
            is_provisoned = False
        time.sleep(2)
    g_mqttc_rpc_processor = mqtt_local()
    g_mqttc_rpc = mqtt_local(broker_host=host_name,broker_port=port,clientid="rpc",on_msg_callback=on_rpc_message, subscribe_topic= TOPIC_RPC_CMD,access_token="")
    
    
    while(True):
        payload = {}
        payload['deviceName'] = get_mac_address() + "-RPC"
        payload['active']=  True
        payload['version'] = "v1.0"
        payload['gatewayName'] = HOSTNAME + "-" + get_mac_address().replace(":", "")[-4:]
        payload['ipAddress'] = get_ip()

        publish_msg(g_mqttc_rpc_processor,topic_pub,payload)
        time.sleep(30) 

if __name__ == "__main__":
    main()


