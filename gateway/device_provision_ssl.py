import ssl
import os
import time
import shutil
#import bluetooth
from datetime import datetime, timedelta
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from paho.mqtt.client import Client
from json import dumps, loads
import configparser
from pathlib import Path
import random
import string
import cherrypy
import threading
import netifaces
import hawk_util as hawk_util
from hawk_util import *
from  hawkeye_logging import get_logger
logger = get_logger(logger_name = __name__, use_color=False)

REBOOT_CMD = "sudo reboot"

RESULT_CODES = {
    1: "incorrect protocol version",
    2: "invalid client identifier",
    3: "server unavailable",
    4: "bad username or password",
    5: "not authorised",
}

REMOTE_COMMAND = {
    "remove_cert" : 0,
    "reboot": 1
}

RESULT = {
    "success" : 0,
    "failure" : 1,
    "invalid" : 2
}


GATEWAY_STOP_CMD = 'sudo service thingsboard-gateway stop'
GATEWAY_START_CMD = 'sudo service thingsboard-gateway start'

g_cert = None
g_key = None

configParser = configparser.ConfigParser()
configParser.read('config.ini')

g_is_server_up = False
#thread_remote_processor = None
g_event_remote_processor =  threading.Event()


def collect_required_data():
    config = {}
    config["host"] = configParser.get('SERVER', 'HOST')
    config["port"] = configParser.getint('SERVER', 'PORT')
    config["provision_device_key"] = configParser.get('SERVER', 'DEVICE_KEY')
    config["provision_device_secret"] = configParser.get(
        'SERVER', 'DEVICE_SECRET')
    config["device_name"] = get_mac_address()
    #config["device_name"] = bluetooth.read_local_bdaddr()[0]
    return config

def generate_certs(ca_certfile=CA_CERT):
    root_cert = None
    try:
        with open(ca_certfile, "r") as ca_file:
            root_cert = x509.load_pem_x509_certificate(str.encode(ca_file.read()), default_backend())
    except Exception as e:
        logger.error("Failed to load CA certificate {}".format(e))
    if root_cert is not None:
        private_key = rsa.generate_private_key(
            public_exponent=65537, key_size=2048, backend=default_backend()
            )
        new_subject = x509.Name([
        x509.NameAttribute(NameOID.COMMON_NAME, "localhost")
    ])
        certificate = (
            x509.CertificateBuilder()
            .subject_name(new_subject)
            .issuer_name(new_subject)
            .public_key(private_key.public_key())
            .serial_number(x509.random_serial_number())
            .not_valid_before(datetime.utcnow())
            .not_valid_after(datetime.utcnow() + timedelta(days=365*10))
            .add_extension(x509.BasicConstraints(ca=True, path_length=None), critical=True)
            .sign(private_key=private_key, algorithm=hashes.SHA256(), backend=default_backend())
        )

        cert_file_content = certificate.public_bytes(encoding=serialization.Encoding.PEM)
        key_file_content = private_key.private_bytes(encoding=serialization.Encoding.PEM,
                                                     format=serialization.PrivateFormat.TraditionalOpenSSL,
                                                     encryption_algorithm=serialization.NoEncryption(),
                                                     )    
        with open(PUBLIC_KEY, "wb") as cert_file:
            cert_file.write(cert_file_content)

        with open(PRIVATE_KEY, "wb") as key_file:
            key_file.write(key_file_content)

        with open(CLIENT_CERT, "wb") as clientcert_file:
            clientcert_file.write(key_file_content)
            clientcert_file.write(cert_file_content)

def read_cert():
    try:
        with open(PUBLIC_KEY, "r") as cert_file:
            cert = cert_file.read()
        with open(PRIVATE_KEY, "r") as key_file:
            key = key_file.read()
    except Exception as e:
        logger.error("Cannot read certificate with error {}".format(e))
    return cert, key


class ProvisionClient(Client):
    PROVISION_REQUEST_TOPIC = "/provision/request"
    PROVISION_RESPONSE_TOPIC = "/provision/response"

    def __init__(self, host, port, provision_request):
        super().__init__()
        self._host = host
        self._port = port
        self._username = "provision"
        self.tls_set(ca_certs=CA_CERT, tls_version=ssl.PROTOCOL_TLSv1_2)
        self.on_connect = self.__on_connect
        self.on_message = self.__on_message
        self.__provision_request = provision_request

    def __on_connect(self, client, userdata, flags, rc):  # Callback for connect
        if rc == 0:
            logger.info("[Provisioning client] Connected to ThingsBoard ")
            client.subscribe(self.PROVISION_RESPONSE_TOPIC)  # Subscribe to provisioning response topic
            provision_request = dumps(self.__provision_request)
            logger.info("[Provisioning client] Sending provisioning request {}".format(provision_request))
            client.publish(self.PROVISION_REQUEST_TOPIC, provision_request)  # Publishing provisioning request topic
        else:
            logger.error("[Provisioning client] Cannot connect to ThingsBoard!, result: {}".format(RESULT_CODES[rc]))

    def __on_message(self, client, userdata, msg):
        decoded_payload = msg.payload.decode("UTF-8")
        decoded_message = loads(decoded_payload)
        provision_device_status = decoded_message.get("status")
        if provision_device_status == "SUCCESS":
            if decoded_message["credentialsValue"] == g_cert.replace("-----BEGIN CERTIFICATE-----\n", "")\
                                                          .replace("-----END CERTIFICATE-----\n", "")\
                                                          .replace("\n", ""):
                logger.info("[Provisioning client] Provisioning success! Certificates are saved.")
                self.__save_credentials(g_cert)
            else:
                logger.error("[Provisioning client] Returned certificate is not equal to sent one.")
        else:
            logger.error("[Provisioning client] Provisioning was unsuccessful with status {} and message: {}".format(provision_device_status, decoded_message["errorMsg"]))
        self.disconnect()

    def provision(self):
        logger.info("[Provisioning client] Connecting to ThingsBoard (provisioning client)")
        self.__clean_credentials()
        #self.connect(self._host, self._port, 60)
        #self.loop_forever()
        
        rc=-1
        while rc!=0:
            try:
                rc = self.connect(self._host, self._port, 60)
                self.loop_forever()
                #self.loop_start() #start a thread to network loop
            except Exception as e:
                logger.error("MQTT connection failed: ".format(str(e)))
                time.sleep(10)

    def get_new_client(self):
        client_credentials = self.__get_credentials()
        new_client = None
        if client_credentials:
            new_client = Client()
            new_client.tls_set(ca_certs="mqttserver.pub.pem", certfile="public_key.pem", keyfile="private_key.pem", cert_reqs=ssl.CERT_REQUIRED,
                               tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
            new_client.tls_insecure_set(False)
            logger.info("[Provisioning client] Read credentials from file.")
        else:
            logger.error("[Provisioning client] Cannot read credentials from file!")
        return new_client

    @staticmethod
    def __get_credentials():
        new_credentials = None
        try:
            with open("credentials", "r") as credentials_file:
                new_credentials = credentials_file.read()
        except Exception as e:
            logger.error("Error {}".format(e))
        return new_credentials

    @staticmethod
    def __save_credentials(credentials):
        with open("credentials", "w") as credentials_file:
            credentials_file.write(credentials)

    @staticmethod
    def __clean_credentials():
        open("credentials", "w").close()


def on_tb_connected(client, userdata, flags, rc):  # Callback for connect with received credentials
    if rc == 0:
        logger.info("[ThingsBoard client] Connected to ThingsBoard with credentials: username: {}, password: {}, client id: {}".format(client._username, client._password, client._client_id))
    else:
        logger.error("[ThingsBoard client] Cannot connect to ThingsBoard!, result: {}".format(RESULT_CODES[rc]))

def check_ssl_cert():
    DPS_TRACKER_FILE = DPS_TRACKER_DIR + CLIENT_CERT
    if os.path.exists(DPS_TRACKER_FILE):
        return True
    else:
        return False

def remove_cert():
    cert_file_remove_cmd = "sudo rm "+ DPS_TRACKER_DIR +"*"

    #return success even if ssl file does not exist as it is considered success from requester perspective
    if check_ssl_cert() is True:
        os.system(cert_file_remove_cmd)
        os.system(GATEWAY_STOP_CMD) #stop the gateway
        res = RESULT["success"]
    else:
        res = RESULT["success"]

    payload={}
    payload['c'] =  REMOTE_COMMAND["remove_cert"]
    payload['r'] =  res
    return dumps(payload)

     

def reboot():
    logger.info("Device Reboot..")
    os.system(REBOOT_CMD)
    #in case of fail only, then response will sent.
    payload={}
    payload['c'] =  REMOTE_COMMAND["reboot"]
    payload['r'] =  RESULT["failure"]
    return dumps(payload)


def main():
    global g_event_remote_processor
    global g_cert
    global g_key

    file_permission_cmd = "sudo chmod 400 /etc/thingsboard-gateway/mqtt*"
    file_ownership_cmd = "sudo chown thingsboard_gateway:thingsboard_gateway /etc/thingsboard-gateway/mqtt*"
    gw_config_path = "/etc/thingsboard-gateway/"

    thread_remote_processor = threading.Thread(name="Webserver_Monitor_T", target=startWebServer)
    thread_remote_processor.start()

    while(True):
        if(check_ssl_cert() is False):
            os.system(GATEWAY_STOP_CMD)
            logger.info("Gateway Server Stopped......")
            config = collect_required_data()

            THINGSBOARD_HOST = config["host"]  # ThingsBoard instance host
            THINGSBOARD_PORT = config["port"]  # ThingsBoard instance MQTT port

            PROVISION_REQUEST = {"provisionDeviceKey": config["provision_device_key"],  # Provision device key, replace this value with your value from device profile.
                                "provisionDeviceSecret": config["provision_device_secret"],  # Provision device secret, replace this value with your value from device profile.
                                "credentialsType": "X509_CERTIFICATE",
                                }
            if config.get("device_name") is not None:
                PROVISION_REQUEST["deviceName"] = config["device_name"]
            src_dir = os.getcwd()    
            if os.path.exists(src_dir +"/"+CLIENT_CERT) is False:    
                generate_certs()  # Generate certificate and key
            else:
                pass    
            g_cert, g_key = read_cert()  # Read certificate and key

            PROVISION_REQUEST["hash"] = g_cert
            if PROVISION_REQUEST.get("hash") is not None:
                provision_client = ProvisionClient(
                    THINGSBOARD_HOST, THINGSBOARD_PORT, PROVISION_REQUEST)
                provision_client.provision()  # Request provisioned data
                tb_client = provision_client.get_new_client() # Getting client with provisioned data
                if tb_client:
                    tb_client.on_connect = on_tb_connected  # Setting callback for connect
                    tb_client.connect(THINGSBOARD_HOST, THINGSBOARD_PORT, 60)
                    tb_client.disconnect() #disconnect the client 
                    #gateway ssl certificate configuration
                    src_dir = os.getcwd()
                    if os.path.exists(gw_config_path):
                        logger.info("SSL configuation started..")
                        shutil.copy(src_dir + "/" + CA_CERT, gw_config_path)
                        shutil.copy(src_dir + "/" + CLIENT_CERT, gw_config_path)
                        os.system(file_permission_cmd)
                        os.system(file_ownership_cmd)
                    time.sleep(1)
                    logger.info("SSL configuation completed..")
                    os.system(GATEWAY_START_CMD)
                    logger.info("Gateway Server Started........")
                    # copy the private key to provision  to provisionse tracker file
                    if os.path.exists(DPS_TRACKER_DIR) is False:
                        os.mkdir(DPS_TRACKER_DIR)
                    shutil.copy(src_dir + "/" + CLIENT_CERT, DPS_TRACKER_DIR)
                    g_event_remote_processor.set()
                else:
                    logger.error("Client was not created!")
            else:
                logger.error("Cannot read certificate.")
        else:
            if g_is_server_up is False: 
                g_event_remote_processor.set()  # start web server
            else:
                pass    
        time.sleep(10)

#web Server 
@cherrypy.expose
class StringGeneratorWebService(object):
    
    @cherrypy.tools.accept(media='application/json')
    def GET(self):
        return cherrypy.session['mystring']

    @cherrypy.tools.accept(media='application/json')
    def POST(self, length=8):
        response = None

        rawData = cherrypy.request.body.read()
        payload = loads(rawData)
        command = payload['c']
        if(command == REMOTE_COMMAND["remove_cert"]):
            response = remove_cert()
        elif(command == REMOTE_COMMAND["reboot"]):
            response = reboot()    
        else:
            response = dumps({'c': command, 'r': RESULT["invalid"]})
        
        return response
        

    def PUT(self, another_string):
        cherrypy.session['mystring'] = another_string

    def DELETE(self):
        cherrypy.session.pop('mystring', None)

def startWebServer():
    global g_is_server_up
    global g_event_remote_processor

    g_event_remote_processor.wait()

    cherrypy.config.update({'server.socket_host': '0.0.0.0','server.socket_port': 9090,})
    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        }
    }
    cherrypy.quickstart(StringGeneratorWebService(), '/', conf)
    g_is_server_up = True
    #g_event_remote_processor.clear()

def printthreadnames():
    for thread in threading.enumerate():
        logger.info("Thread Name: {}".format(thread.name))

if __name__ == '__main__':
    main()
    printthreadnames()    
