#!/bin/bash

username="hawkeye"
password="hawkeye@123"


#change the keyboard setting to "us", default is "uk"
sed  -i "s/\(XKBLAYOUT*= *\).*/\1us/" /etc/default/keyboard

#install openssl, required for password setting.
sudo apt-get install openssl
#add hawkeye user and add sudo group
sudo useradd -m "$username" -G sudo
sudo usermod -a -G sudo "$username"
# set the hawkeye password(default) as hawkeye@123 
sudo usermod --password $(echo "$password" | openssl passwd -1 -stdin) "$username"
echo "$username user created successfully."
sudo chsh -s /bin/bash "$username"

#change the hostname
sudo hostnamectl set-hostname $username
sudo sed -i 's/kali/hawkeye/g' /etc/hosts


#reboot the device
sudo reboot
