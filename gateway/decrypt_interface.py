import subprocess
import re
import netifaces
import pandas as pd
import traceback
import sys
import schedule
import time
import os
import paho.mqtt.client as mqtt
import time
import sys
import json
import os
import hawk_mqtt as hawk_mqtt
from hawk_mqtt import *
import hawk_util as hawk_util
from hawk_util import *
import ipaddress
import netifaces
from  hawkeye_logging import get_logger
import threading
from collections import OrderedDict
from queue import Queue
import multiprocessing


host_name = '127.0.0.1'
port = 1883
qos = 1
topic_pub = '/hawkeye/data'
logger = get_logger(logger_name = __name__, use_color=False)
ip_network = None
LRU_CACHE_CAPACITY = 500  # LRU Cache capacity
TX_WAIT_IN_SECS = 60  # wait period for sending same src/dst info

g_tap_interface = None 

# LRU Cache for tracking src and dst ip addresses
class LRUCache:
    # initialising capacity
    def __init__(self, capacity: int):
        self.cache = OrderedDict()
        self.capacity = capacity
 
    # we return the value of the key
    # that is queried in O(1) and return -1 if we
    # don't find the key in out dict / cache.
    # And also move the key to the end
    # to show that it was recently used.
    def get(self, key):
        if key not in self.cache:
            return None
        else:
            self.cache.move_to_end(key)
            return self.cache[key]
 
    # first, we add / update the key by conventional methods.
    # And also move the key to the end to show that it was recently used.
    # But here we will also check whether the length of our
    # ordered dictionary has exceeded our capacity,
    # If so we remove the first key (least recently used)
    def put(self, key, value) -> None:
        self.cache[key] = value
        self.cache.move_to_end(key)
        if len(self.cache) > self.capacity:
            self.cache.popitem(last = False)
 
def start_airtun(bssid,password,wireless_network):
    """
    start airtun
    """
    try:
        command = (
        'sudo', 'airtun-ng', '-a', f'{bssid}', '-p', f'{password}', '-e', f'{wireless_network}',
        'wlan0mon')
        # subprocess.run(command, stdout=subprocess.PIPE)
        
        resp = subprocess.Popen(command, preexec_fn=os.setsid)
        with open("PID.txt", "w") as fb:
            fb.write(str(os.getpgid(resp.pid)))

    except:
        logger.error("starting airtun Failed")
        traceback.print_exc()


def start_tap_interface():
    '''
    start virtual interface generated from airtun
    :return:
    '''
    global g_tap_interface
    try:
        command = (
            f'sudo tshark -D'
        )
        resp = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, errors = resp.communicate()
        list_ssid = output.decode("utf-8")
        logger.info("interface: {}".format(list_ssid.split()))
        l = []
        for iface in list_ssid.split():
            if iface.startswith("at"):
                l.append(iface)
        logger.info("tunnel interface running: {}".format(l))
        g_tap_interface = l[-1]
        commands = (f'sudo ifconfig  {l[-1]} up')
        logger.info("running {} command".format(commands))
        resp = subprocess.Popen(commands, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, errors = resp.communicate()
        if errors != None:
            logger.error(" Command {} has error {}".format(command,errors))
    except:
        logger.error("Failed to start tap interface")
        traceback.print_exc()

def publish_msg(mqttc,topic, payload):
    #g_mqttc_local = mqtt_local()

    #print("type of payload is " + str(payload))
    payload = json.dumps(payload)     # python object --> json string
    try:
        rc, mid = mqttc.publish(topic=topic, payload=payload, qos=0, retain=False)
        if rc != mqtt.MQTT_ERR_SUCCESS:
            logger.error("publish error. {}".format(mqtt.error_string(rc)))
        else:
            logger.debug("published on topic {} msg {}  with rc {}".format(topic, str(payload), str(rc) ))

    except Exception as e:
        logger.error("publish error e={}".format(e))


def on_local_publish(client, userdata, mid):
    logger.debug("disconnect mqqt client")
    client.disconnect()        
    #pass

def msg_publisher(queue_tx_payload):
    mqttc = mqtt_local()
    while True:
        tx_payload = queue_tx_payload.get()    # this is a blocking call
        #logger.info(tx_payload)
        payload = tx_payload["payload"]
        topic = tx_payload["topic"]
        
        #mqttc.on_publish = on_local_publish
        publish_msg(mqttc,topic, payload)
        logger.debug("Published Payload: {}".format(payload))
        time.sleep(0.1) # give time for context switching

def tshark_output_procesor(stdout,queue_tx_payload,notification_email):
        global ip_network
        message_counter = 1
        src_dst_ip_tracker = OrderedDict()
        dns_ip_tracker = LRUCache(LRU_CACHE_CAPACITY)
        device_ip = get_ip()
        subnet_mask='.'.join(device_ip.split('.')[:-1]+["0"])+'/24'
        src_os_indicator = {}        
        while True:
        #for stdout_line in iter(popen.stdout):
            
            stdout_line = stdout.readline()
            logger.debug("Received: {}".format(stdout_line))
            logger.debug("Message Counter: {}".format(message_counter))
            data = None
            try:
                data = json.loads(stdout_line)
            except (json.decoder.JSONDecodeError,ValueError):
                logger.error("Received invalid JSON packet")
                continue

            if 'layers' in data.keys() and 'dns' in data['layers'].keys():
                dns_frame = data['layers']['dns']
                logger.debug("DNS Frame:{}".format(dns_frame))
                if 'dns_dns_qry_name' in dns_frame.keys() and 'dns_dns_a' in dns_frame.keys():
                    dns_ips = dns_frame['dns_dns_a']
                    dns_host = dns_frame['dns_dns_qry_name']
                    if isinstance(dns_ips, str):
                        dns_ip_tracker.put(dns_ips,dns_host)
                    elif isinstance(dns_ips, list):
                        for dns_ip in dns_ips:
                            dns_ip_tracker.put(dns_ip,dns_host)
                    else:
                        pass    
                logger.debug("DNS Tracker : {}".format(dns_ip_tracker))

            if 'index' not in data and len(data['layers']) != 0 and 'http' in data['layers'] and 'eth' in data['layers']:
                http_frame = data['layers']['http']    
                eth_frame = data['layers']['eth']
                logger.debug("http frame: {}  eth frame: {}".format(http_frame, eth_frame))
                eth_src = eth_frame['eth_eth_addr']
                if 'http_http_user_agent' in http_frame:
                    user_agent = http_frame['http_http_user_agent']
                    logger.info("User Agent:{}".format(user_agent))
                    if 'Android' in user_agent:
                        src_os_indicator[eth_src] = "Android Mobile"
                    elif 'iPhone' in user_agent:
                        src_os_indicator[eth_src] = "Apple Mobile"
                    elif 'Linux' in user_agent:
                        if eth_src in src_os_indicator.keys(): 
                            logger.debug("os indicator within linux agent:{} eth_src: {}".format(src_os_indicator, eth_src))
                            if "Android" not in src_os_indicator[eth_src]: # for Android mobiles, user agent keeps on toggling between Linux and Android
                                src_os_indicator[eth_src] = "Linux PC"
                        else:
                            src_os_indicator[eth_src] = "Linux PC"

                    elif 'Windows' in user_agent:
                        src_os_indicator[eth_src] = "Windows PC"
                    else:
                        pass
                        #src_os_indicator[eth_src] = "Unknown"
                logger.debug("OS Indicator: {}".format(src_os_indicator))

            #{"timestamp":"1639459712169","layers":{"ip_src_host":["74.125.200.188"],"ip_dst_host":["192.168.1.3"],"eth_src":["38:94:e0:dd:0b:35"],"eth_dst":["46:cf:d5:c1:56:e3"]}}
            if 'index' not in data and len(data['layers']) != 0 and ('ip' in data['layers'] and 'eth' in data['layers']) :
                src = None
                dst = None
                device_eth = None
                dns_ip = None
                eth_frame = data['layers']['eth']
                ip_frame = data['layers']['ip']
                logger.debug("Data Frame: {}".format(data))
                payload = {}
                if 'timestamp' in data:
                    payload['timestamp'] = data['timestamp']
                if 'ip_ip_src_host' in ip_frame:    
                    src = ip_frame['ip_ip_src_host']
                    payload['ip_src_host'] = src
                if 'ip_ip_dst_host' in ip_frame:     
                    dst = ip_frame['ip_ip_dst_host']
                    payload['ip_dst_host'] = dst
                
                # get http frame
                '''
                if 'http' in data['layers'].keys():
                    http_frame = data['layers']['http']
                    
                    if 'http_http_host' in http_frame:
                        payload['dns_qry_name'] = http_frame['http_http_host']
                    elif 'http_http_response_for_uri' in http_frame:
                        payload['dns_qry_name'] = http_frame['http_http_response_for_uri']
                    else:
                        payload['dns_qry_name'] = ""   
                
                if 'dns_qry_name' in data['layers']:
                    payload['dns_qry_name'] = data['layers']['dns_qry_name'][0]
                else:
                    payload['dns_qry_name'] = ""     
                '''

                if 'eth_eth_addr' in eth_frame:
                    payload['eth_src'] = eth_frame['eth_eth_addr']
                if 'eth_eth_dst' in eth_frame:
                    payload['eth_dst'] = eth_frame['eth_eth_dst']    
                
                if src is not None and dst is not None :
                    #for filter MAC Adress from IP 
                    src_address = ipaddress.ip_address(src)
                    a_network = ipaddress.ip_network(subnet_mask)
                    dest_address = ipaddress.ip_address(dst)

                    if src_address in a_network and dest_address not in a_network:
                        device_eth = eth_frame['eth_eth_addr']
                        dns_ip = dst
                        payload['serialNumber'] =  get_mac_address() +"-"+ device_eth 
                        payload['ipAddress'] = src
                    
                    if dest_address in a_network and src_address not in a_network:
                        device_eth = eth_frame['eth_eth_dst']
                        dns_ip = src
                        payload['serialNumber'] =  get_mac_address() +"-"+ device_eth
                        payload['ipAddress'] = dst


                #logger.info("Paylod: {}".format(payload))
                #data_payload = json.dumps(payload)
                publish_msg_payload = False
                if "serialNumber" in payload:
                    payload['deviceType'] = 'hawkeye-node'
                    logger.debug("src_os_indicator: {} eth: {}".format(src_os_indicator,device_eth))
                    if device_eth in src_os_indicator:
                        payload['type'] =  src_os_indicator[device_eth] 
                    else:  
                        payload['type'] = 'Unknown'
                    payload['notification_email'] = notification_email
                    
                    logger.debug("DNS IP: {}  DNS Tracker : {}".format(dns_ip,dns_ip_tracker))

                    dns_hostname = dns_ip_tracker.get(dns_ip)

                    if dns_hostname is not None:
                        logger.debug("dns_hostname:{}".format(dns_hostname))
                        payload['dns_qry_name'] = dns_hostname
                    else:
                        payload['dns_qry_name'] = dns_ip

                    # Create/Update tracker dictionary
                    if src is not None:
                        logger.debug("src:{} dst:{}".format(src,dst))
                        if src in src_dst_ip_tracker:
                            dst_addresses_cache = src_dst_ip_tracker[src]
                            if dst is not None:
                                dst_present = dst_addresses_cache.get(dst)
                                if dst_present is None:
                                    ts = time.time()
                                    dst_addresses_cache.put(dst,ts)
                                    src_dst_ip_tracker[src] = dst_addresses_cache
                                    publish_msg_payload = True 
                                    logger.debug("New Destination added:{}".format(dst_addresses_cache))
                                else:
                                    ts = dst_addresses_cache.get(dst)
                                    current_ts = time.time()
                                    logger.debug("Time difference: {}".format(current_ts-ts))
                                    if(current_ts-ts) > TX_WAIT_IN_SECS:
                                        publish_msg_payload = True 
                                        dst_addresses_cache.put(dst,current_ts)
                                        src_dst_ip_tracker[src] = dst_addresses_cache
                                        logger.debug("Time difference: {}".format(current_ts-ts))
                                        logger.debug("Should be published.............")
                                    

                        else:
                            # create LRU Cache
                            dst_cache = LRUCache(LRU_CACHE_CAPACITY)
                            if dst is not None:
                                # ts stores the time in seconds
                                ts = time.time()
                                dst_cache.put(dst,ts)
                            src_dst_ip_tracker[src]= dst_cache
                            publish_msg_payload = True 
                            logger.debug("New Entry being created....")

                    if publish_msg_payload  is True :
                        publish_topic = topic_pub+"/"+str(message_counter)
                        payload_dict = {}
                        payload['ts'] = int(time.time() * 1000)
                        payload_dict["topic"] = publish_topic
                        payload_dict["payload"] = payload

                        if queue_tx_payload.full():
                            queue_tx_payload.get()
                            logger.info("Payload Queue is full. Removing oldest payload from Queue")
                        queue_tx_payload.put(payload_dict)  # append to Queue
                        
                        logger.debug("queue_tx_payload: {}".format(queue_tx_payload.qsize()))
                        logger.info("Published Message Counter: {}".format(message_counter))
                        message_counter = message_counter + 1
                        time.sleep(0.1)	
                else:
                    pass
            else:
                pass  
            time.sleep(0.1)
        logger.info("Exiting the tshark message processor thread!!!!")

def tshark_publish(bssid,email):
    '''
    tshark to publish at0 interface messages
    '''
    global g_tap_interface
    queue_tx_payload=Queue(maxsize = 500)  # we do not expect more than this at any point of time
    try:

        ''' 
        command = (
            'tshark', '-i', f'{g_tap_interface}', '-J', 'tcp http', '-T', 'fields', '-e', 'ip.src_host', '-e', 'ip.dst_host', '-e',
            'dns.qry.name', '-e','eth.src','-e','eth.dst','-e','http.host','-e','http.user_agent','-T', 'json',
            '-T', 'ek')
        '''
        command = (
            'tshark', '-i', f'{g_tap_interface}', '-T', 'json',
            '-T', 'ek')
        
        #command = ('tshark', '-i', f'{g_tap_interface}', '-T', 'json', '-T', 'ek') # get all the fields here
        deauth_command = (
            'aireplay-ng', '-0', '1' ,'-a', f'{bssid}',  'wlan0mon')

        logger.info("Running tshark Command: {}".format(command))
        popen = subprocess.Popen(command, stdout=subprocess.PIPE, preexec_fn=os.setsid)
        logger.info("Running deauth Command: {}".format(deauth_command))
        popen_deauth = subprocess.Popen(deauth_command, stdout=subprocess.PIPE, preexec_fn=os.setsid)
        stdout_processor_thread = threading.Thread(name="tshark_T", target=tshark_output_procesor, args=(popen.stdout,queue_tx_payload,email))
        stdout_processor_thread.start()
        publisher_thread = threading.Thread(name="mqtt_publisher_T", target=msg_publisher , args=(queue_tx_payload,))
        publisher_thread.start()
    except:
        logger.error("starting tshark and publish Failed")
        traceback.print_exc()

def remove_tap_interface():
    """
    remove interface with interface name
    startswith
    """

    try:
        interface_list = netifaces.interfaces()
        logger.debug("Network interface", interface_list)
        for networks in interface_list:
            if networks.startswith("at"):
                command = (
                    f'sudo ifconfig {networks} down'
                )
                logger.info("running {} command".format(command))
                resp = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                        start_new_session=True)
                output, errors = resp.communicate()
                if errors != None:
                    logger.error(" Command {} has error {}".format(command,errors))

    except:
        logger.error("tap interface  Failed")
        traceback.print_exc()

def init():
    try:

        x, y = kill_process()
        parse_inputconf()
        try:
            commands = []
            if x is not None:
                commands.append(f'sudo kill -9 {x[0]}')

            if y is not None:
                commands.append(f'sudo kill -9 {y[0]}')

            for command in commands:
                logger.info("running {} command".format(command))
                resp = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                logger.info("Process id for command : {} is  {}".format(command,os.getpgid(resp.pid)))
                output, errors = resp.communicate()
                if errors != None:
                    logger.error( "Command {} has error {}".format(command,errors))


        except IndexError:
            logger.error("iNDEX eRROR")
            pass
    except:
        logger.error("Failed to start in Monitoring Mode")
        traceback.print_exc()

def kill_process():
    l = ["PID.txt","PID_tshark.txt"]
    siz = []
    x = None
    y = None
    for files in l:
        if os.path.exists(files):
            siz.append(os.path.getsize(files))

    if 0 in siz:
        return None,None

    else:
        PID_txt = "PID.txt"
        if os.path.exists(PID_txt):
            with open(PID_txt, "r") as fb:
                x = fb.readlines()
                logger.info("pid kill{} len{}".format(x, len(x)))
        PID_tshark_txt = "PID_tshark.txt"
        if os.path.exists(PID_tshark_txt):
            with open(PID_tshark_txt, "r") as fb:
                y = fb.readlines()
                logger.info("pid kill{} len {}".format(y, len(y)))

    return x, y

def parse_inputconf():
    '''
    parse inputs from JSON
    :return:
    '''
    try:
        with open("configuration.json", "r") as fb:
            json_data = json.loads(fb.read())

        
        wireless_network = json_data["wpa_credentials"][0].get("ssid")
        wireless_interface = json_data["wireless_interface"]
        bssid = json_data["bssid"]

        password = json_data["wpa_credentials"][0].get("password")
        notification_email = json_data["notification_email"]
    except:
        logger.error("failed to parse input JSON")
        traceback.print_exc()

    return wireless_network,wireless_interface,bssid,password,notification_email

def main():
    #check for device provisoned 
    is_provisoned = False
    while is_provisoned is False:
        logger.debug("WiFi Configuration is not found.")
        DPS_TRACKER_FILE = DPS_TRACKER_DIR + CLIENT_CERT
        CONFIG_TRACKER_FILE = src_dir +"/"+CONFIG_FILE 
        if os.path.exists(DPS_TRACKER_FILE) and os.path.exists(CONFIG_TRACKER_FILE):
            is_provisoned = True
        else:
            is_provisoned = False
        time.sleep(2)
    time.sleep(10)

    try:
        wireless_network,wireless_interface,bssid,password,notification_email = parse_inputconf()  # get configuration
        init()
        remove_tap_interface()
        p1 = multiprocessing.Process(target=start_airtun, args=(bssid,password,wireless_network,))
        p1.start()
        time.sleep(2)
        start_tap_interface()
        p2 = multiprocessing.Process(target=tshark_publish, args=(bssid,notification_email,))
        p2.start()
    
    except:
        print("Application Failed")
        traceback.print_exc()

if __name__ == "__main__":
    main()
    while True:  # keep looping here
        logger.info("Waiting in loop!!")
        time.sleep(600) # heartbeat check
    

