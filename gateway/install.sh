#!/bin/bash

#relative path
script_relative_path=$(dirname $(realpath "$0"))

#update the library
sudo apt-get update

#install dependencies packages
sudo apt-get install -f

#create dps directory and copy device provision files
config_file="config.ini"
public_cert_file="mqttserver.pub.pem"
provision_script_file="device_provision_ssl.py"
channel_monitoring_script="channel_monitoring.py"
decrypt_interface_script="decrypt_interface.py"
logger_file="hawkeye_logging.py"
hawk_mqttc_file="hawk_mqtt.py"
rpc_shell="rpc_shell.py"
hawk_util="hawk_util.py"
#for tracking process, later this will remove
file1="PID_tskark.txt"
file2="PID.txt"
#user_home_dir=$(eval echo ~$USER)
user="hawkeye"
#change the hostname
#sudo hostnamectl set-hostname $user
#sudo sed -i 's/kali/hawkeye/g' /etc/hosts
user_home_dir=$(sudo -u $user sh -c 'echo $HOME')
provision_dir_name="dps"
if [ -d "$user_home_dir"/"$provision_dir_name" ]; then
    echo "Directory is already exist"
else
    mkdir "$user_home_dir"/"$provision_dir_name"
fi  
cp "$script_relative_path"/"$config_file" "$script_relative_path"/"$public_cert_file" "$script_relative_path"/"$provision_script_file" "$script_relative_path"/"$channel_monitoring_script" "$script_relative_path"/"$decrypt_interface_script" "$script_relative_path"/"$logger_file" "$script_relative_path"/"$hawk_mqttc_file" "$script_relative_path"/"$rpc_shell" "$script_relative_path"/"$hawk_util" "$user_home_dir"/"$provision_dir_name"/

cp "$script_relative_path"/"$file1" "$script_relative_path"/"$file2" "$user_home_dir"/"$provision_dir_name"/

#install thingsboard gateway
#wget https://github.com/thingsboard/thingsboard-gateway/releases/latest/download/python3-thingsboard-gateway.deb
#sudo apt install ./python3-thingsboard-gateway.deb -y
sudo apt install "$script_relative_path"/python3-thingsboard-gateway.deb -y
sudo service thingsboard-gateway stop
#copy the tb gateway config file
tb_gw_conf="tb_gateway.yaml"
sudo cp "$script_relative_path"/"$tb_gw_conf" /etc/thingsboard-gateway/config/
#copy the tb hawkeye connector file
hawkeye_connector="hawkeye_mqtt.json"
sudo cp "$script_relative_path"/"$hawkeye_connector" /etc/thingsboard-gateway/config/
sudo service thingsboard-gateway start

#install supervisor service
echo "Installation of the Mosquitto server..."
sudo apt install mosquitto -y

#install supervisor service
echo "Installation of the supervisor server..."
sudo apt install supervisor

#Enable supervisor service on system startup
sudo systemctl enable supervisor 

echo "Starting of the supervisor server..."
sudo service supervisor start

#install python lib for cherrypy(web serve)
echo "Installation of the cherrypy package..."
sudo pip3 install cherrypy

#install python lib for mqtt paho
echo "Installation of the mqtt paho package..."
sudo pip3 install paho-mqtt

#Enable mosquitto broker service on system startup
sudo systemctl enable mosquitto

echo "Configure the supervisor server..."
filename="provision_supervisor.conf" 
sudo service supervisor stop
sleep 2
#remove default config filesystemctl enable supervisord
supervisor_conf="/etc/supervisor/conf.d/*.conf"
if [  -f "$supervisor_conf" ]
then
    echo "$0: File '${supervisor_conf}' not found."
    sudo rm /etc/supervisor/conf.d/*
fi
sudo cp "$script_relative_path"/"$filename" /etc/supervisor/conf.d/
sudo service supervisor start
echo "supervisor server configure successfully...!!"



#for network monitoring and decrypt script dependencies
sudo pip3 install pyrcrack
sudo pip3 install schedule

#delete the default user
sudo userdel -r kali
echo "Default user[kali] deleted successfully ."


sudo reboot
