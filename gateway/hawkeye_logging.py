import logging
import logging.handlers
import sys

import configparser

config = configparser.ConfigParser()
config.read('config.ini')

#FORMAT = "%(asctime)s %(name)s: [%(levelname)s] %(funcName)s():%(lineno)d %(message)s [%(threadName)s]"
#FORMAT = "%(asctime)s [%(name)s] %(levelname)8s: %(message)s. [%(funcName)s():%(lineno)d] [%(process)d/%(threadName)s]"
FORMAT_BOLD = "\033[1m%(asctime)s\033[0m %(levelname)8s: %(message)s.  [%(filename)s|%(funcName)s|%(lineno)d] [%(process)d/%(threadName)s]"
FORMAT1 = "%(asctime)s %(levelname)8s: %(message)s.  [%(filename)s|%(funcName)s|%(lineno)d] [%(threadName)s]"
FORMAT = "%(asctime)s %(levelname)8s: %(message)s. [%(filename)s|%(funcName)s|%(lineno)d] [%(process)d/%(threadName)s]"

formatter4console = logging.Formatter(FORMAT)
formatter4file = logging.Formatter(FORMAT1)
logging_level = config.get('logging','level')

def get_console_handler():
   console_handler = logging.StreamHandler(sys.stdout)
   console_handler.setFormatter(formatter4console)
   return console_handler

def get_file_handler():
    LOG_FILE = config.get('logging','logfile')
    file_handler = logging.handlers.TimedRotatingFileHandler(LOG_FILE, when='midnight')
    file_handler.setFormatter(formatter4file)
    return file_handler

def get_syslog_handler():
    # sudo gedit /etc/rsyslog.conf
    # tail -n 2 /var/log/syslog
    try:
        #handler = logging.handlers.SysLogHandler(address='var/log', facility=syslog.LOG_USER)
        handler = logging.handlers.SysLogHandler(address='var/log')
    except EnvironmentError:
        handler = logging.handlers.SysLogHandler()

    #formatter = logging.Formatter('Python: { "loggerName":"%(name)s", "timestamp":"%(asctime)s", "pathName":"%(pathname)s", "logRecordCreationTime":"%(created)f", "functionName":"%(funcName)s", "levelNo":"%(levelno)s", "lineNo":"%(lineno)d", "time":"%(msecs)d", "levelName":"%(levelname)s", "message":"%(message)s"}')
    formatter = logging.Formatter("vq_solution: %(asctime)s, %(pathname)s, %(funcName)s, %(lineno)d, %(levelname)s, %(message)s")
    handler.setFormatter(formatter)
    return handler

def get_logger(logger_name, use_color=False):
    logger = logging.getLogger(logger_name)
    logger.setLevel(config.get('logging','level'))      # debug is the lowest built-in severity level
   
    #if config.get('logging','tosyslog'):
    #    logger.addHandler(get_syslog_handler())
        #logger.info("Test Message")

    #if config.get('logging','tofile'):
    #    logger.addHandler(get_file_handler())

    if config.get('logging','toconsole'):
        logger.addHandler(get_console_handler())
  
    # with this pattern, it's rarely necessary to propagate the error up to parent
    logger.propagate = False

    # Add color
    #echo -e "Normal texst \033[1;31mred bold text\033[0m normal text again"
    RESET_SEQ = "\033[0m"
    COLOR_SEQ = "\033[1;%dm"
    BOLD_SEQ  = "\033[1m"
    if  (use_color and not config.get('logging','tofile')) :
        logging.addLevelName( logging.WARNING, "\033[1;33m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
        logging.addLevelName( logging.DEBUG,   "\033[1;35m%s\033[1;0m" % logging.getLevelName(logging.DEBUG))
        logging.addLevelName( logging.ERROR,   "\033[1;41m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
        logging.addLevelName( logging.INFO,   "\033[1;32m%s\033[1;0m" % logging.getLevelName(logging.INFO))
    #logging.addLevelName( logging.INFO,   "\033[1;32m%s" % logging.getLevelName(logging.INFO))
    return logger


##--------------------------------------------------------



# https://www.toptal.com/python/in-depth-python-logging
