# hawk-eye

Codename for home network monitoring product being developed for asmltd and polylogyx. Hawk-eye can see visible light as well as ultraviolet light, which is not visible. This project aims at detecting visible as well hidden threats in home network 

# soho-monitor
This contains mobile app source code developed in flutter. Open this project in Android Studio to build and debug

# gateway
This contains code for hawkeye device for device configuration & provisioning and will have code for monitoring as well. Most of the code is in python with scripts being run at startup through supervisor 

# thingsboard
This contains IOT Platform source. This is derived from version 3.2.2 released on 24-Mar-2021
For build use command: mvn clean install -DskipTests -Dlicense.skip=true

# installers
This contains apk and installer binaries for platform

