# soho_monitor

Home Network Monitoring Application

## Getting Started
    Install nymea network manager on raspberry pi 4 using following commands. Create installation script later on to simplify installation
- echo "deb http://repository.nymea.io buster rpi" | sudo tee /etc/apt/sources.list.d/nymea.list
- sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key A1A19ED6
- sudo apt-get update
- sudo apt-get install nymea-networkmanager dirmngr
- sudo systemctl disable dhcpcd
- reboot

## Coding Guidelines
    https://dart-lang.github.io/linter/lints/index.html
## Dart Cheatsheat
    https://dart.dev/codelabs/dart-cheatsheet
## Troubleshooting
- Emulator not starting with /dev/kvm device:permission denied error
	sudo chmod 777 -R /dev/kvm
- Flutter-blue errors
    flutter upgrade
- Device scan does not see any hawkeye devices
    Ensure Bluetooth is not connected to any other device
    HawkEye application has permission to access location    