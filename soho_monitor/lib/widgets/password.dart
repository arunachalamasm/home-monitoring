// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class PasswordTextField extends StatefulWidget {
  const PasswordTextField(
      {Key? key, required this.controller, this.keyboardType, this.maxLength, this.decorationText})
      : super(key: key);

  final TextEditingController controller;
  final TextInputType? keyboardType;
  final int ? maxLength;
  final String ? decorationText;

  @override
  _PasswordTextFieldState createState() => new _PasswordTextFieldState();
}
class _PasswordTextFieldState extends State<PasswordTextField> {
  bool _showPassword = false;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      obscureText: !_showPassword,
      keyboardType: widget.keyboardType ?? null,
      maxLength: widget.maxLength ?? null,
      decoration: InputDecoration(
        labelText: widget.decorationText?? "Password",
        //prefixIcon: Icon(Icons.security_outlined),
        suffixIcon: new GestureDetector(
          onTap: () {
            setState(() {
              _showPassword = !_showPassword;
            });

          },
          child:
          new Icon(_showPassword ? Icons.visibility : Icons.visibility_off, ),
        ),
      ),
    );
  }

}
