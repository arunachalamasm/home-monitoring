// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/services/ucs/ucs.dart';

Future <String> createUser(String userId) async{
  String _loginURL = serverUrl;  // default to login url of dashboard
  UCSClient  ucsClient = new UCSClient();
  UserCreationResponse response = await ucsClient.add(userId);
  if (response == UserCreationResponse.success) {
    AppSettings.instance.setUserId(userId);
    _loginURL = ucsClient.getActivationURL();
  }
  return _loginURL;
}


Future <bool> showEmailDialog(BuildContext context) async {
  bool next = false;
  String? currentUserId = AppSettings.instance.userId;
  final textController = TextEditingController();
  if(currentUserId == null)
    textController.text = "";
  else
    textController.text = currentUserId;

  next = await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Enter Email Id",
          style: Theme.of(context).textTheme.subtitle1,),
        content: TextField(
          controller: textController,
        ),
        actions: <Widget>[
          new TextButton(
            child: new Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          new TextButton(
            child: Text("Next"),
            onPressed: () async {
              await createUser(textController.text);
              Navigator.of(context).pop(true);
            },
          ),
        ],
      );
    },
  );
  return next;
}