// Copyright 2021, mSense Inc
// All rights reserved.



import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:soho_monitor/models/access_point.dart';
import 'dart:async';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/services/rcs/rcs.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/services/dcs/dcs.dart';
import 'package:soho_monitor/services/wifi/btwifi_setup.dart';
import 'package:soho_monitor/services/wifi/wifi_service.dart';
import 'package:soho_monitor/services/wifi/wifi_services.dart';

Future <bool> forgetNetwork(String btId) async{

  DCSClient  dcsClient = new DCSClient();
  DeviceCreationResponse response = await dcsClient.delete(btId,AppSettings.instance.userId);
  if (response == DeviceCreationResponse.deviceNotFound) {
    print('Device not Found');
  }

  WiFiService? _wifiService = await WifiServices.instance.getService(btId);
  if(_wifiService != null) {
    int _retries = 5; // Add retries for write to handle occasional setnotify failures
    while (_retries > 0) {
      try {
        WifiAccessPoint? ap = await _wifiService.getCurrentWiFiConnection();
        if (ap !=null) {
          RCSClient rcsClient = new RCSClient(ap.hostAddress);
          RemoteServiceResponse response = await rcsClient.removeCert();
          if (response.index == RemoteServiceResponse.success.index) {
            await _wifiService.disconnectWifi();
            await _wifiService.disconnect();
            AppSettings.instance.deleteDevice(btId);
            return true;
          }
        }
        _retries = 0;
      } catch (e) {
        _retries--;
        print('WifiAccessPoint read Failed');
      }
    }
  } else {
    print("Service/Device not available");
  }
  return false;
}

Future <RemoveDevice> showDeleteDialog(BuildContext context, String btId) async {

  String? deviceName = AppSettings.instance.getDeviceName(btId);
  String title = "Remove " + ((deviceName!=null)?deviceName:"Device");
  bool isChecked = false;
  RemoveDevice bForceRemove = RemoveDevice.donotRemove;

  await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (BuildContext context) {
      return StatefulBuilder(builder: (context,setState){
        return AlertDialog(
          title: Text(title,
            style: Theme.of(context).textTheme.subtitle1,),
          content: Form (
            child: Column(
                mainAxisSize:  MainAxisSize.min,
                children:[
                  Text("Are you sure you want to remove the device?",
                    style: Theme.of(context).textTheme.caption,),
                  Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        Checkbox(value: isChecked, onChanged: (checked){
                          setState((){
                            isChecked  = checked!;
                          });
                        }),
                        Text("Force Remove",
                          style: Theme.of(context).textTheme.caption,)
                      ]
                  )
                ]
            )),
            actions: <Widget>[
              new TextButton(
                child: Text("Cancel"),
                onPressed: () {
                  bForceRemove = RemoveDevice.donotRemove;
                  Navigator.of(context).pop(false);
                },
              ),
              new TextButton(
                child: Text("OK"),
                onPressed: () async {
                  //forgetNetwork(btId);
                  if(isChecked){
                    bForceRemove = RemoveDevice.forceRemove;
                  } else{
                    bForceRemove = RemoveDevice.remove;
                  }
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        },
        );
      });
  return bForceRemove;
}

Future  showDeleteStatusDialog(BuildContext context, String btId, RemoveDevice bForceRemove) async {
  bool ret = false;
  bool deviceDeleted = false;
  bool autoCloseDialog = false;
  StateSetter? updateStatus;
  String statusText = "Removing device...";
  String buttonText = "Cancel";
  bool deviceLinked =  false;

  Timer _statusTimer = Timer(Duration(seconds: 1), () async {

    DCSClient  dcsClient = new DCSClient();
    DeviceCreationResponse response = await dcsClient.delete(btId, AppSettings.instance.userId);
    if ((response == DeviceCreationResponse.deviceNotFound) && (bForceRemove != RemoveDevice.forceRemove)) {
        //print('Device not Found');
        statusText = "Device not Found!!";
        buttonText = "OK";
        updateStatus!(() {});

    }

    if ((response == DeviceCreationResponse.networkError) && (bForceRemove != RemoveDevice.forceRemove)) {
        statusText = "Please check your internet connection and try again!!";
        buttonText = "OK";
        updateStatus!(() {
      });
    }
    else{
      statusText= "Connecting to device";
      updateStatus!(() {
      });
      WiFiService? _wifiService = await WifiServices.instance.getService(btId);
      if(_wifiService != null) {
          BluetoothDeviceState connectionState = BluetoothDeviceState.disconnected;
          if (bForceRemove == RemoveDevice.forceRemove) {
            connectionState = await _wifiService.getConnectionStatus();
          }
          if ((bForceRemove == RemoveDevice.forceRemove) &&  (connectionState == BluetoothDeviceState.disconnected))
          {
            // do nothing
          }
          else
            {
              int _retries = 5; // Add retries for write to handle occasional setnotify failures
              while (_retries > 0) {
                try {
                  statusText = "Getting connection info";
                  updateStatus!(() {});
                  WifiAccessPoint? ap = await _wifiService
                      .getCurrentWiFiConnection();
                  if (ap != null) {
                    statusText = "Resetting device credentials from HawkEye";
                    updateStatus!(() {});
                    print(ap.hostAddress);
                    RCSClient rcsClient = new RCSClient(ap.hostAddress);
                    RemoteServiceResponse response = await rcsClient.removeCert();
                    // If there is network error here, it means most likely web server is not running. Proceed with force removal of device
                    if ((response.index == RemoteServiceResponse.success.index) ||
                        (response.index ==
                            RemoteServiceResponse.networkError.index)) {
                      await _wifiService.disconnectWifi();
                      await _wifiService.disconnect();
                      statusText = "Delinking device";
                      updateStatus!(() {});
                      AppSettings.instance.deleteDevice(btId);
                      autoCloseDialog = true;
                      deviceDeleted = true;
                    }
                  }
                  _retries = 0;
                } catch (e) {
                  _retries--;
                  await Future.delayed(Duration(seconds: 1));
                  print('WifiAccessPoint read Failed');
                }
              }
          }

      } else {
        if (bForceRemove != RemoveDevice.forceRemove){
          print("Service/Device not available");
          statusText="Device not in range. Please ensure you are near device and try again!!";
          buttonText = "OK";
          updateStatus!(() {});
        }
      }
      if ((bForceRemove == RemoveDevice.forceRemove)){
        AppSettings.instance.deleteDevice(btId);
        autoCloseDialog = true;
        deviceDeleted = true;
      }
    }

  });
  await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (BuildContext context) {
      return StatefulBuilder(builder: (context, StateSetter setState){
        updateStatus = setState;
        if(autoCloseDialog){
          _statusTimer.cancel();
          Navigator.of(context).pop(deviceDeleted);
        }
        return AlertDialog(
          title: Text("Remove Device",
            style: Theme.of(context).textTheme.subtitle1,),
          content:  Text(statusText + "...",
            style: Theme.of(context).textTheme.caption,),

          actions: <Widget>[
            new TextButton(
              child: new Text(buttonText),
              onPressed: () {
                _statusTimer.cancel();
                Navigator.of(context).pop(deviceLinked);
              },
            ),
          ],
        );
      });
    },
  );

  //return ret;
}
