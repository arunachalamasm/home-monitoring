// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/widgets.dart';
import 'package:soho_monitor/screens/homepage/homescreen.dart';


final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  "/": (BuildContext context) => HomeScreen(),
};
