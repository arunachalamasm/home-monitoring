
import 'package:http/http.dart' as http;
import 'package:soho_monitor/models/constants.dart';
import 'dart:async';
import 'dart:convert';


class UCSClient {
  var _dcsUrl = Uri.parse(serverUrl + "/ucs");
  var _userActivationUrl = "";
  Future<UserCreationResponse> add(String? emailId) async {
    UserCreationResponse returnStatus = UserCreationResponse.failed;
    var _request = new Map();
    _request['command'] = "add";
    var _parameters =  new Map();
    _parameters['tenantName'] = tenantName;
    _parameters['dashboardName'] = dashboardName;
    _parameters['emailId'] = emailId;

    _request['parameters'] = _parameters;

    String payload = jsonEncode(_request);
    try {
      final response = await http.post(_dcsUrl,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': 'application/json'
        },
        body: payload,);
      Map<String, dynamic> jsonResponse = jsonDecode(response.body);
      //return jsonResponse['message'];
      if (response.statusCode == 200) {
        _userActivationUrl = jsonResponse['activationUrl'];
        returnStatus =  UserCreationResponse.success;
      }
      else if (response.statusCode == 409) {
        returnStatus =  UserCreationResponse.userExists;
        _userActivationUrl = jsonResponse['activationUrl'];
      }
      else {
        returnStatus = UserCreationResponse.failed;
      }
    } catch ( error ){
      // This exception can happen in case of network error
      returnStatus = UserCreationResponse.networkError;
    }
    return returnStatus;
  }

  String getActivationURL(){
    return _userActivationUrl;
  }
  Future<UserCreationResponse> delete( String emailId) async {
    UserCreationResponse returnStatus = UserCreationResponse.failed;
    var _request = new Map();
    _request['command'] = "delete";

    var _parameters =  new Map();
    _parameters['tenantName'] = tenantName;
    _parameters['dashboardName'] = dashboardName;
    _parameters['emailId'] = emailId;

    _request['parameters'] = _parameters;

    String payload = jsonEncode(_request);
    try {
      final response = await http.post(_dcsUrl,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': 'application/json'
        },
        body: payload,);

      if (response.statusCode == 200){
        returnStatus = UserCreationResponse.success;
      }
      else if (response.statusCode == 404){
        returnStatus = UserCreationResponse.userNotFound;
      }
      else{
        returnStatus = UserCreationResponse.failed;
      }
    } catch(error){
      returnStatus = UserCreationResponse.networkError;
    }

    return returnStatus;
  }
}
