
import 'package:http/http.dart' as http;
import 'package:soho_monitor/models/constants.dart';
import 'dart:async';
import 'dart:convert';


class LoginClient {
  var loginUrl = Uri.parse(serverUrl + "/api/auth/login");

  Future<String> fetchToken(String username, String password) async {
    print("Login URL: $loginUrl");

    final response = await http.post(loginUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'username': username,
        'password': password
      }),);

    final String responseString = response.toString();
    print("Response: $responseString");
    if (response.statusCode != 200) {
      throw new Exception('error getting quotes');
    }
    Map<String, dynamic> jsonResponse = jsonDecode(response.body);
    return jsonResponse['token'];
  }
}
