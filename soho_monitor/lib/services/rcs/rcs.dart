import 'package:http/http.dart' as http;
import 'package:soho_monitor/models/constants.dart';
import 'dart:async';
import 'dart:convert';

class RCSClient {
  final String serverURL;

  RCSClient(this.serverURL);

  Future<RemoteServiceResponse> removeCert() async {
    RemoteServiceResponse status = RemoteServiceResponse.failed;
    var rcsUrl = Uri.parse("http://" + serverURL + ":9090");
    var _request = new Map();
    final command = RemoteServiceCommand.removeCert.index;
    _request['c'] = command;

    String payload = jsonEncode(_request);
    try {
      final response = await http.post(
        rcsUrl,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': 'application/json'
        },
        body: payload,
      );
      Map<String, dynamic> jsonResponse = jsonDecode(response.body);
      print(jsonResponse);
      if (jsonResponse['r'] == RemoteServiceResponse.success.index) {
        status = RemoteServiceResponse.success;
      } else if (jsonResponse['r'] == RemoteServiceResponse.failed.index) {
        status = RemoteServiceResponse.failed;
      } else {
        status = RemoteServiceResponse.invalidCommand;
      }
    } catch(error){
      status = RemoteServiceResponse.networkError;
    }
    return status;
  }

  Future<RemoteServiceResponse> reboot() async {
    var rcsUrl = Uri.parse("http://" + serverURL + ":9090");
    var _request = new Map();
    final command = RemoteServiceCommand.reboot.index;
    _request['c'] = command;
    String payload = jsonEncode(_request);
    final response = await http.post(
      rcsUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json'
      },
      body: payload,
    );

    Map<String, dynamic> jsonResponse = jsonDecode(response.body);
    if (jsonResponse['r'] == RemoteServiceResponse.success.index) {
      return RemoteServiceResponse.success;
    } else if (jsonResponse['r'] == RemoteServiceResponse.failed.index) {
      return RemoteServiceResponse.failed;
    } else {
      return RemoteServiceResponse.invalidCommand;
    }
  }
}
