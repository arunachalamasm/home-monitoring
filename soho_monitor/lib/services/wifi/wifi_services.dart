// Copyright 2021, mSense Inc
// All rights reserved.


import 'dart:collection';

import 'package:flutter_blue/flutter_blue.dart';

import 'package:soho_monitor/services/wifi/wifi_service.dart';

class WifiServices {
  final LinkedHashMap _wifiServicesMap = new LinkedHashMap<String, WiFiService>();  // For Tracking wifiservice for a device

  /// Singleton boilerplate
  WifiServices._(){
    //_init();
  }
  static WifiServices _instance = new WifiServices._();
  static WifiServices get instance => _instance;
  /// Retrieve a list of linked devices
  Future<WiFiService?> getService(String btId) async {
    WiFiService? _wifiService;
    print(_wifiServicesMap);
    print(btId);
    if(_wifiServicesMap.containsKey(btId)){
      WiFiService wifiService = _wifiServicesMap[btId];
      _wifiService = wifiService;
    }
    else{
      BluetoothDevice? btDevice = await WiFiService.findBtDevice(btId);
      if (btDevice != null) {
        WiFiService wifiService = WiFiService(btDevice);
        _wifiServicesMap.putIfAbsent(btId, () => wifiService);
        _wifiService = wifiService;
      }
    }
    return _wifiService;
  }
}
