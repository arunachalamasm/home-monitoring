// Copyright 2021, mSense Inc
// All rights reserved.

import 'dart:async';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:soho_monitor/models/access_point.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/services/wifi/btwifi_setup.dart';
import 'package:rxdart/rxdart.dart';

class WiFiService{
  late BtWifiSetup _service;
  BehaviorSubject<bool> _isScanning = BehaviorSubject.seeded(false);
  Stream<bool> get isScanning => _isScanning.stream;

  WiFiService(BluetoothDevice device)
  {
    _service = new BtWifiSetup(device);
  }

  static Future <BluetoothDevice?> findBtDevice(String btId) async {
    // Return device if found
    return await BtWifiSetup.findBtDevice(btId);
  }
  Future <bool> connect() async {
    int _retries = 3; //make 3 attempts for connect
    bool _ret = false;
    while (_retries > 0) {
      _ret = await _service.connectToDevice();
      if (_ret) // break from retries if setup successful
        {
        _isScanning.add(true);
          await _service.scanWiFi();
        _isScanning.add(false);
          break;
        }
      _retries--;
    }
    if(_ret == false){
        print("Connection Failed!!!");
      }
    return _ret;
  }

  Future<void> disconnect() async {
    await _service.disconnectFromDevice();
  }

  Future<WirelessStatus> getWiFiConnectionStatus() async {
    return await _service.getWiFiConnectionStatus();
  }

  Future<BluetoothDeviceState> getConnectionStatus() async {
    return await _service.getConnectionStatus();
  }

  Future <void> configureWifi(String ssid , String password) async {
    _isScanning.add(true);  // this is for simulating screen refresh. Find a better replacement later
    await _service.connectDeviceToWiFi(ssid, password);
    // Start timer for stopping scan state
    Timer(Duration(seconds: 30), () {
        _isScanning.add(false);
      });
    WifiAccessPoint? ap;
    while(ap == null && _isScanning.value == true) {
      ap = await _service.getCurrentConnection();
      //await _service.getWiFiConnectionStatus();
      await Future.delayed(Duration(seconds: 1));
    }
    _isScanning.add(false);
  }

  Future <void> disconnectWifi() async{
    _isScanning.add(true);  // this is for simulating screen refresh. Find a better replacement later
    await _service.disconnectDeviceFromWiFi();
  }

  Future <WifiAccessPoint?> getCurrentWiFiConnection() async{
    return await _service.getCurrentConnection();
  }

  BluetoothDevice getBtDeviceInfo(){
    return _service.getBtDevice();
  }

  Future <List<WifiAccessPoint>> startWiFiScan(Duration? timeout) async {
    _isScanning.add(true);
    // Start timer for stopping scan state
    if (timeout != null) {
      Timer(timeout, () {
        _isScanning.add(false);
      });
    }
    List<WifiAccessPoint> _accesspoints = [];
    _accesspoints = await _service.scanWiFi();
    _isScanning.add(false);

    return _accesspoints;
  }
  void stopWiFiScan(){
    _isScanning.add(false);
  }

  Stream<List <WifiAccessPoint>> get accessPoints async* {
    while (true) {
      List <WifiAccessPoint> _accesspoints = _service.getAccessPoints();
      //print("Accesspoints: $_accesspoints");
      /*if(_accesspoints.isNotEmpty) {
        yield _accesspoints;
        await getCurrentWiFiConnection();
      }
      else{
        //WifiAccessPoint? ap = await getCurrentWiFiConnection();  // get Current Connection
       // if(ap!=null){
         // _accesspoints.add(ap);
          //yield _accesspoints;
       // }
      }*/
      //print("Accesspoints: $_accesspoints  Scanning: ${_isScanning.value}");
      if (_isScanning.value == true)
      {
        yield [];
      }
      else{
        if(_accesspoints.isNotEmpty) {
          yield _accesspoints;
          await getCurrentWiFiConnection();  // get Current Connection
        }
      }
      await Future.delayed(Duration(seconds: 1));
    }
  }
}
