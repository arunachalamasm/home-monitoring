// Copyright 2021, mSense Inc
// All rights reserved.

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'package:validators/validators.dart'; // Required for isJSON validation
import 'package:flutter/cupertino.dart'; // Required for Debug Print
import 'package:collection/collection.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/models/access_point.dart';


// Main class for handling request/response to and from nymea network manager
// This has been implemented taking cues from berrylan app source code and mapping it with
// flutter_blue

class BtWifiSetup {
  BluetoothDevice _device;
  FlutterBlue _flutterBlue = FlutterBlue.instance;
  bool _enableLogs = true;
  final String advertisingName = "HawkEye";
  List<BluetoothService> _services = [];
  BluetoothService? get _wifiService => _services.firstWhereOrNull((element) => element.uuid.toString() == wifiServiceUuid);

  BluetoothCharacteristic? get _wifiRequest =>
      _wifiService?.characteristics.firstWhereOrNull((element) => element.uuid.toString() == wifiCommanderCharacteristicUuid);

  BluetoothCharacteristic? get _wifiResponse =>
      _wifiService?.characteristics.firstWhereOrNull((element) => element.uuid.toString() == wifiResponseCharacteristicUuid);

  BluetoothCharacteristic? get _wifiConnectionStatus =>
      _wifiService?.characteristics.firstWhereOrNull((element) => element.uuid.toString() == wifiStatusCharacteristicUuid);

  LinkedHashMap _accessPointsHashMap = new LinkedHashMap<String, WifiAccessPoint>();  // For Tracking changes in WifiAccess Points for current connection
  BtWifiSetup(this._device);
  // Connect to Hawkeye Device
  Future <bool> connectToDevice() async {

    try {
      final state = await getConnectionStatus();
      if (state == BluetoothDeviceState.disconnected) {
        // stop scanning before starting a new connection
        if (await _flutterBlue.isScanning.first) {
          await _flutterBlue.stopScan();
        }
        await _device.connect(autoConnect: true);
        _services = await _device.discoverServices();
        await _wifiResponse?.setNotifyValue(true);
        return true;
      }else if (state == BluetoothDeviceState.connected){
        return true;
      } else{  // Device is in either connecting or disconnecting state. Wait and return so that application can retry
        await Future.delayed(Duration(seconds: 2));
        return false;
      }

    } catch(e){
      _log("Connect Failed!!!");
      return false;
    }
  }

  // Disconnect from Hawkeye Device
  Future <void> disconnectFromDevice() async{
    await _device.disconnect();
  }

  // Disconnect from Hawkeye Device
  Future <WirelessStatus> getWiFiConnectionStatus() async{
    WirelessStatus ret = WirelessStatus.unknown;
    await connectToDevice();  // ensure there is connection before sending any command
    // if characteristic response is null, rediscover the services otherwise return error
    if(_services.isEmpty){
      _services = await _device.discoverServices();
    }

    int _retries = 5;  // Add retries for write to handle occasional setnotify failures
    while(_retries >0) {
      try {
        List<int> ? status = await _wifiConnectionStatus?.read();  // Read the current status value
        if(status != null)
          ret = WirelessStatus.values[status[0]];
        _retries = 0;
      } catch(e){
        _retries--;
        // wait for a second before trying next read attempt
        await Future.delayed(Duration(seconds: 1));
        _log('_wifiConnectionStatus Read Status Failed');
      }
    }

   print(" Wifi Connection Status : $ret");
   return ret;

  }

  // Set Wifi ssid and password on Hawkeye to connect to Wifi
  Future <void> connectDeviceToWiFi(String ssid, String password) async {
    _accessPointsHashMap.clear();  // Clear current list so that we see only current connection
    var _request = new Map();
    final command = WirelessServiceCommand.connect.index;
    _request['c'] = command;

    var _parameters =  new Map();
    _parameters['e'] = ssid;
    _parameters['p'] = password;

    _request['p'] = _parameters;
    await validateServices();  // Cal this to attempt getting all the services
    final response = await _processTxRx(_request, _wifiRequest, _wifiResponse, command: command);
    return response;
  }

  // Disconnect Wifi on Hawkeye Device
  Future <void> disconnectDeviceFromWiFi() async{
    // disconnect Hawkeye from currently connected network
    _accessPointsHashMap.clear();  // Clear accesspoints list to repopulate to avoid showing the current connection as connected even after disconnect  as this
    var _request = new Map();
    final command = WirelessServiceCommand.disconnect.index;
    _request['c'] = command;

    await validateServices();  // Cal this to attempt getting all the services
    final response = await _processTxRx(_request, _wifiRequest, _wifiResponse, command: command);
    return response;
  }

  // get access points
  // get access points list
  List <WifiAccessPoint> getAccessPoints()  {
    // Iterate through Hashmap to update Accesspoint List. We need to ensure that
    // the list has connected access point at the beginning
    final _accessPointsList = <WifiAccessPoint>[];
    _accessPointsHashMap.forEach((ssid, accessPoint) {
      WifiAccessPoint ap = accessPoint;
      if (ap.hostAddress.isEmpty)
        _accessPointsList.add(ap);
      else
        _accessPointsList.insert(0, ap);
    });
    return _accessPointsList;
  }

  // get currently connected accesspoint
  Future <WifiAccessPoint?> getCurrentConnection()  async {
      return await _getCurrentConnection();  // Trigger request so that this is updated in next call
  }

  // get currently connected accesspoint
  BluetoothDevice getBtDevice()  {
    return _device;
  }

  // find bluetooth device with given btid in range
  static Future <BluetoothDevice?> findBtDevice(String btId)  async {
    final completer = Completer<BluetoothDevice?>();
    late StreamSubscription subscription;
    BluetoothDevice? foundDevice;
    FlutterBlue _flutterBlue = FlutterBlue.instance;

    final connectedDevices = await _flutterBlue.connectedDevices;
    final device = connectedDevices.firstWhereOrNull((element) => element.id.toString().contains(btId));// Check if this device is found
    if (device == null) {  //  device is not found in connected devices list scan to see if device is in range
      // Listen to scan results
      subscription = _flutterBlue.scanResults.listen((results) async {

        final wantedDevice = results.firstWhereOrNull((element) => element.device.id.toString().contains(btId));
        if (wantedDevice != null) {
          // Stop scanning
          await _flutterBlue.stopScan();
          subscription.cancel();
          completer.complete(wantedDevice.device);
        }

      });
      // Start scanning
      if (await _flutterBlue.isScanning.first) {
        await _flutterBlue.stopScan();
      }

      await _flutterBlue.startScan(timeout: Duration(seconds: 5)).whenComplete(() {
        if(!completer.isCompleted){
          subscription.cancel();
          completer.complete(foundDevice);
        }
      });


    } else {
      completer.complete(device);
    }

    return completer.future;
  }


  // Scan for available Wifi Newtorks on Hawkeye Device
  Future<List<WifiAccessPoint>> scanWiFi() async{
    return await _getNetworks();
  }

  // Local Functions
  Future<BluetoothDeviceState> getConnectionStatus() async {
    BluetoothDeviceState _connectionStatus = BluetoothDeviceState.disconnected;
    //check connection is successful. This may not be correct use of await for but since we expect only one value, we will live with it for now
    await for (var state in _device.state) {
      _connectionStatus = state;
      return _connectionStatus;
    }
    return _connectionStatus;
  }

  Future <void> validateServices() async {
    await connectToDevice();  // ensure there is connection before sending any command
    // if characteristic response is null, rediscover the services otherwise return error
    if(_services.isEmpty){
      _services = await _device.discoverServices();
    }

    int _retries = 5;  // Add retries for write to handle occasional setnotify failures
    while(_retries >0) {
      try {
        await _wifiResponse?.setNotifyValue(true);  // Ensure this is set to avoid not getting any response. Revisit this later as this is not optimal
        _retries = 0;
      } catch(e){
        _retries--;
        // wait for a second before trying next set notify attempt
        await Future.delayed(Duration(seconds: 1));
        _log('SetNotify failed');
      }
    }

  }

  Future<dynamic> _processTxRx(Map request, BluetoothCharacteristic? characteristicRequest, BluetoothCharacteristic? characteristicResponse, {int? command}) async {
    late StreamSubscription subscription;
    Completer completer = Completer();
    if (characteristicResponse == null) {
        return Future.error(StateError('no characteristicResponse for command $command'));
    }

    var completeData = '';
    subscription = characteristicResponse.value.listen((element) {
      if (element.isNotEmpty) {
        final partData = String.fromCharCodes(element);
        completeData += partData;
        if (completeData.contains('\n')) {
          if(isJSON(completeData) == false)  // Sometimes flutter blue sends repeated responses with some characters lost. check it is a valid json before processing
            return;
          final response = jsonDecode(completeData);
          // response isn't for the given command, skip the response
          if (command != null && command != response['c']) {
            return;
          }
          _log('response for command $command: ${response.toString()}');

          if (response['r'] == 0) {
            completer.complete(response['p']);
          } else {
            completer.completeError(ResponseException(response));
          }
          subscription.cancel();
        }
      }
    });
    final rawData = '${jsonEncode(request)}\n'.codeUnits;

    final packets = (rawData.length / 20).ceil();
    for (var i = 0; i < packets; i++) {
      final endIndex = i == packets - 1 ? rawData.length : i * 20 + 20;
      final part = rawData.sublist(i * 20, endIndex);
      _log('Write part of $i/$packets: ' + String.fromCharCodes(part));
      int _retries = 5;  // Add retries for write to handle occasional write failures
      while(_retries >0) {
        try {
          await characteristicRequest!.write(part);
          _retries = 0;
        } catch(e){
          _retries--;
          // wait for a second before trying next write attempt
          await Future.delayed(Duration(seconds: 1));
          _log('Write characteristic failed');
        }
      }
    }

    return completer.future;
  }
  // Scan for available Wifi Networks on Hawkeye Device
  Future<void> _scanNetworks() async{
    var _request = new Map();
    final command = WirelessServiceCommand.scan.index;
    _request['c'] = command;
    await validateServices();  // Cal this to attempt getting all the services
    _log("Characteristics: Request: $_wifiRequest  Response: $_wifiResponse Service : $_wifiService  BTServices: $_services");
    final response = await _processTxRx(_request, _wifiRequest, _wifiResponse, command: command);
    return response;
  }

  /// Returns a list of [WifiAccessPoint] available networks on the remote device
  Future<List<WifiAccessPoint>> _getNetworks() async {
    _accessPointsHashMap.clear();
    await _scanNetworks();
    var _request = new Map();
    final command = WirelessServiceCommand.getNetworks.index;
    _request['c'] = command;
    await validateServices();  // Cal this to attempt getting all the services
    final response = await _processTxRx(_request, _wifiRequest, _wifiResponse, command: command);
    final networks = <WifiAccessPoint>[];
    response.forEach((network)  {
      if (network['e'].toString().trim().isNotEmpty){ // Check it is not hidden network
        WifiAccessPoint ap = WifiAccessPoint.fromJson(network);
        // filter hidden networks
        if(ap.ssid.contains('Hidden') == false){
          networks.add(ap);
          // put into hash map
          // since the mac being reported is of connected access point and not device. Store as per ssid to avoid duplicates
          _accessPointsHashMap.putIfAbsent(ap.ssid, () => ap);
        }
      }
    });
    _log("discovered networks: $networks");
    return networks;
  }

  Future <WifiAccessPoint?> _getCurrentConnection() async{
    var _request = new Map();
    final command = WirelessServiceCommand.getCurrentConnection.index;
    _request['c'] = command;
    await validateServices();  // This to attempt getting all the services
    final response = await _processTxRx(_request, _wifiRequest, _wifiResponse, command: command);
    WifiAccessPoint currentConnection = WifiAccessPoint.fromJson(response);
    if(currentConnection.hostAddress.isNotEmpty){ // If there is host address then only process this request
    if(_accessPointsHashMap.containsKey(currentConnection.ssid))
      _accessPointsHashMap.remove(currentConnection.ssid); // Remove the existing entry
    _accessPointsHashMap.putIfAbsent(currentConnection.ssid, () => currentConnection);
    return currentConnection;
    }
  }

  void _log(String message) {
    if (_enableLogs) {
      debugPrint(message);
    }
  }

  @override
  String toString() {
    return '{ ${this._device} }';
  }
}

class DeviceNotFoundException implements Exception {

  @override
  String toString() {
    return 'Device Not found{}';
  }
}

class ResponseException implements Exception {
  final Map<String, dynamic>? rawResponse;

  ResponseException(this.rawResponse);

  @override
  String toString() {
    return 'ResponseException{rawResponse: $rawResponse}';
  }
}
