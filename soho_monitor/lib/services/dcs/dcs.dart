
import 'package:http/http.dart' as http;
import 'package:soho_monitor/models/constants.dart';
import 'dart:async';
import 'dart:convert';


class DCSClient {
  var dcsUrl = Uri.parse(serverUrl + "/dcs");

  Future<DeviceCreationResponse> add(String deviceId, String? emailId) async {
    DeviceCreationResponse returnStatus = DeviceCreationResponse.failed;
    var _request = new Map();
    _request['command'] = "add";
    var _parameters =  new Map();
    _parameters['deviceName'] = deviceId;
    _parameters['tenantName'] = tenantName;
    _parameters['deviceType'] = deviceType;
    if(emailId !=null)
      _parameters['emailId'] = emailId;

    _request['parameters'] = _parameters;

    String payload = jsonEncode(_request);
    try {
      final response = await http.post(dcsUrl,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': 'application/json'
        },
        body: payload,);
      //Map<String, dynamic> jsonResponse = jsonDecode(response.body);
      //return jsonResponse['message'];
      if (response.statusCode == 200) {
        returnStatus =  DeviceCreationResponse.success;
      }
      else if (response.statusCode == 409) {
        returnStatus =  DeviceCreationResponse.deviceExists;
      }
      else {
        returnStatus = DeviceCreationResponse.failed;
      }
    } catch ( error ){
      // This exception can happen in case of network error
      returnStatus = DeviceCreationResponse.networkError;
    }
    return returnStatus;
  }

  Future<DeviceCreationResponse> delete( String deviceId, String? emailId) async {
    DeviceCreationResponse returnStatus = DeviceCreationResponse.failed;
    var _request = new Map();
    _request['command'] = "delete";

    var _parameters =  new Map();
    _parameters['deviceName'] = deviceId;
    _parameters['tenantName'] = tenantName;
    _parameters['deviceType'] = deviceType;
    if(emailId !=null)
      _parameters['emailId'] = emailId;

    _request['parameters'] = _parameters;

    String payload = jsonEncode(_request);
    try {
    final response = await http.post(dcsUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json'
      },
      body: payload,);

    if (response.statusCode == 200){
      returnStatus = DeviceCreationResponse.success;
    }
    else if (response.statusCode == 404){
      returnStatus = DeviceCreationResponse.deviceNotFound;
    }
    else{
      returnStatus = DeviceCreationResponse.failed;
    }
    } catch(error){
      returnStatus = DeviceCreationResponse.networkError;
    }

    return returnStatus;
  }
}
