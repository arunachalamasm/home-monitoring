// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:soho_monitor/models/access_point.dart';
import 'package:soho_monitor/models/constants.dart';

// Parser for received responses from nymea network manager
class CommanderResponse {
  WirelessServiceCommand? command;
  WirelessServiceResponse? responseCode;
  List<WifiAccessPoint> accessPoints;
  WifiAccessPoint? currentConnection;
  CommanderResponse(this.command, this.responseCode, this.currentConnection, [this.accessPoints=const []]);

  factory CommanderResponse.fromJson(dynamic json) {
    WirelessServiceCommand _command = WirelessServiceCommand.values[json['c'] as int];
    WirelessServiceResponse _responseCode = WirelessServiceResponse.values[json['r'] as int];
    CommanderResponse _responseObj = CommanderResponse(_command,_responseCode,null);

    if (_responseCode == WirelessServiceResponse.success) {
      // Process response inline
      switch (_command) {
        case WirelessServiceCommand.getNetworks:
          // update Wireless Access points List
          if (json['p'] != null) {
            var accessPointList = json['p'] as List;
            List<WifiAccessPoint> _accessPoints = [];
            /* Iterate through list to create WifiAccessPoint Object*/
            accessPointList.forEach((accessPoint) {
              // check for hidden networks in the list. If present, filter it
              if (accessPoint['e'] != ""){
                _accessPoints.add(WifiAccessPoint.fromJson(accessPoint));
              }
            });
            //List<WifiAccessPoint> _accessPoints = accessPointList.map((tagJson) => WifiAccessPoint.fromJson(tagJson)).toList();
            _responseObj = CommanderResponse(_command, _responseCode,null, _accessPoints);
          }
          break;
        case WirelessServiceCommand.connect:
          // Do nothing
          break;
        case WirelessServiceCommand.connectHidden:
          // Do nothing
          break;
        case WirelessServiceCommand.getCurrentConnection:
        // update Wireless Access points List

          if (json['p'] != null) {
            var accessPoint = json['p'];
            if (accessPoint['e'] != ""){  // If it is empty response do not process
              WifiAccessPoint _accessPoint = WifiAccessPoint.fromJson(accessPoint);
              _responseObj = CommanderResponse(_command,_responseCode,_accessPoint,[]);
            }
          }
          break;
        case WirelessServiceCommand.scan:
          break;
        case WirelessServiceCommand.disconnect:
          // Do nothing
          break;
        case WirelessServiceCommand.startAccessPoint:
          // Do nothing
          break;
        default:
          print('Invalid Command');
      }
    }
    return _responseObj;
  }

  @override
  String toString() {
    return '{ ${this.command}, ${this.responseCode}, ${this.accessPoints} }';
  }
}
