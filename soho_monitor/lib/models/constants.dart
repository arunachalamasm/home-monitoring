// Copyright 2021, mSense Inc
// All rights reserved.

// Define for Bluetooth Service Ids
// These service Ids are as per nymea network manager requirements

final wifiServiceUuid                     = "e081fec0-f757-4449-b9c9-bfa83133f7fc";
final wifiCommanderCharacteristicUuid     = "e081fec1-f757-4449-b9c9-bfa83133f7fc";
final wifiResponseCharacteristicUuid      = "e081fec2-f757-4449-b9c9-bfa83133f7fc";
final wifiStatusCharacteristicUuid        = "e081fec3-f757-4449-b9c9-bfa83133f7fc";
final wifiModeCharacteristicUuid          = "e081fec4-f757-4449-b9c9-bfa83133f7fc";

final networkServiceUuid                  = "ef6d6610-b8af-49e0-9eca-ab343513641c";
final networkStatusCharacteristicUuid     = "ef6d6611-b8af-49e0-9eca-ab343513641c";
final networkCommanderCharacteristicUuid  = "ef6d6612-b8af-49e0-9eca-ab343513641c";
final networkResponseCharacteristicUuid   = "ef6d6613-b8af-49e0-9eca-ab343513641c";
final networkingEnabledCharacteristicUuid = "ef6d6614-b8af-49e0-9eca-ab343513641c";
final wirelessEnabledCharacteristicUuid   = "ef6d6615-b8af-49e0-9eca-ab343513641c";

// Enum Defines for BT Wifi Setup Usage
enum NetworkStatus { unknown, asleep, disconnected, disconnecting, connecting, local, connectedSite, global }

enum WirelessDeviceMode { unknown, adhoc, infrastructure, accessPoint }

enum WirelessStatus { unknown, unmanaged, unavailable, disconnected, prepare, config, needAuth, ipConfig, ipCheck, secondaries, activated, deactivating, failed }

enum WirelessServiceCommand { getNetworks, connect, connectHidden, disconnect, scan, getCurrentConnection, startAccessPoint }

enum WirelessServiceResponse {success, invalidCommand, invalidParameters, networkManagerNotAvailable, wirelessNotAvailable, wirelessNotEnabled, networkingNotEnabled, unknownError }

enum NetworkServiceCommand { enableNetworking, disableNetworking, enableWireless, disableWireless }

enum NetworkServiceResponse {  success, invalidValue, networkManagerNotAvailable, wirelessNotAvailable, unknownError }

enum DeviceCreationResponse { success, failed, deviceExists, deviceNotFound, networkError }
enum UserCreationResponse { success, failed, userExists, userNotFound, networkError }
enum DeviceLinkingStatus { success, failed, deviceExists, deviceNotFound, networkError }
enum RemoteServiceCommand { removeCert, reboot }

enum RemoteServiceResponse { success, failed, invalidCommand, networkError }

enum RemoveDevice {forceRemove, remove, donotRemove}

final serverUrl = "https://hawkeye.msenseiot.com";

final tenantName = "ASM";

final dashboardName = "HawkEyeDashboard";

final deviceType = "hawkeye";

final List<String> wirelessStatusString = [
  "WiFi connection status unknown on device",
  "Device not managed by network manager",
  "Wifi not available for use on device",
  "Device not connected to WiFi network",
  "Preparing connection to network",
  "Associating with WiFi accesspoint",
  "Authentication failed",
  "Requesting IP address",
  "Validating IP address",
  "Waiting for secondary connection",
  "Device connected to WiFi accesspoint",
  "Disconnecting from current WiFi network",
  "Device connection failed"
  ];

// Define Configuration Constants
final useZeroConfDiscovery = true;
final useBiometrics = true;
