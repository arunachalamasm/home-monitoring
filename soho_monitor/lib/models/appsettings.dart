// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class AppSettings {
  final String _passCodeKey = "pin";
  final String _linkedDevicesKey = "linkedDevices";
  final String _userEmailId = "emailId";
  SharedPreferences ? _prefs;
  /// Singleton boilerplate
  AppSettings._(){
    _init();
  }

  _init() async {
    _prefs = await SharedPreferences.getInstance();
  }


  static AppSettings _instance = new AppSettings._();
  static AppSettings get instance => _instance;

  /// Retrieve a list of linked devices
  List<String>? get linkedDevices {
    return _prefs?.getStringList(_linkedDevicesKey);
  }

  /// Retrieve current set passcode
  String? get passCode {
    return _prefs?.getString(_passCodeKey);
  }

  /// Retrieve device name for a given Id
  String? getDeviceName(String id){
    _prefs?.getString(id);
  }

  /// Retrieve  user email id
  String? get userId {
    return _prefs?.getString(_userEmailId);
  }

  void setPassCode(String passCode){
    _prefs?.setString(_passCodeKey,passCode);
  }

  void setUserId(String userEmailId){
    _prefs?.setString(_userEmailId,userEmailId);
  }

  void setLinkedDevices(List<String> linkedDevices){
    _prefs?.setStringList(_linkedDevicesKey,linkedDevices);
  }

  /// Set device name for a given Id
  void setDeviceName(String id, String name){
    _prefs?.setString(id,name);
  }

  /// remove device from global store
  void deleteDevice(String id){
    _prefs?.remove(id);  // name,id pair removed
    List <String>? btDevices = _prefs?.getStringList(_linkedDevicesKey);

    if(btDevices == null){
      //Unexpected Error
    }
    else{
      // Remove this device
      int deleteIndex = -1;
      int currentIndex = 0;
      btDevices.forEach((btDevice) {
        final json = jsonDecode(btDevice);
        if (json['btId'] == id)
        {
          deleteIndex = currentIndex;
        }
        currentIndex = currentIndex +1;
      });
      if (deleteIndex !=-1) {
        btDevices.removeAt(deleteIndex);
      }
      _prefs?.setStringList(_linkedDevicesKey,btDevices);
    }
  }

  /// reset pin to defaults. Removing the passcode will reset to app defaults
  void resetPassCode(){
    _prefs?.remove(_passCodeKey);
  }

  ///  remove user
  void removeUser(){
    _prefs?.remove(_userEmailId);
  }
  
  /// rename device, HawkEye + last 4 char of Bluetooth ID
  String getUniqueName(String btId) {
    String btid_str = btId.replaceAll(RegExp('[^A-Za-z0-9]'), '');
    String id = btid_str.substring(btid_str.length - 4);
    return "HawkEye"+ "-" + id ;
  }
}
