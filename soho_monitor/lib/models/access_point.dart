// Copyright 2021, mSense Inc
// All rights reserved.

// Accesspoint parser from Json string
class WifiAccessPoint {
  String ssid;
  String mac;
  int signalStrength;
  bool protected;
  String hostAddress;

  WifiAccessPoint(this.ssid, this.mac, this.signalStrength, this.protected,this.hostAddress);

  factory WifiAccessPoint.fromJson(dynamic json) {
    String _ssid = "Hidden";
    String _mac = json['m'] as String;
    String _hostAddress ="";
    int _signalStrength = json['s'] as int;
    bool _protected = json['p'] as int ==1? true:false;
    if (json['e'] != "")
      _ssid = json['e'];

    if (json['i'] != null)
      _hostAddress = json['i'];

    return WifiAccessPoint(_ssid,_mac,_signalStrength,_protected, _hostAddress);
  }
  @override
  String toString() {
    return '{ ${this.ssid}, ${this.mac}, ${this.signalStrength}, ${this.protected} , ${this.hostAddress} }';
  }
}
