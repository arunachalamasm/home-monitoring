// Copyright 2021, mSense Inc
// All rights reserved.


class DeviceInfo {
  final String name;
  final String btId;

  DeviceInfo({required this.name, required this.btId});
  DeviceInfo.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        btId = json['btId'];

  Map<String, dynamic> toJson() => {
    'name': name,
    'btId': btId
  };

  @override
  String toString() {
    return '{ ${this.name}, ${this.btId} }';
  }
}
