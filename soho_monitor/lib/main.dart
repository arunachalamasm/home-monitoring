// Copyright 2021, mSense Inc
// All rights reserved.
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:soho_monitor/screens/authentication/auth.dart';
import 'package:soho_monitor/routes.dart';
import  'package:soho_monitor/widgets/app_lock.dart';
import 'package:soho_monitor/widgets/app_restart.dart';
import 'package:wifi_iot/wifi_iot.dart';

import 'models/appsettings.dart';

void main() async {
  //runApp(SohoMonitoringApp());
  WidgetsFlutterBinding.ensureInitialized(); // This required to avoid crash in accessing SharedPreferences instances
  await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  await Permission.camera.request();
  await Permission.locationWhenInUse.request();
  // instantiate global settings store
  AppSettings.instance;
  runApp(  // Restart app enables restart of the app  from any screen
      RestartApp(
          child: AppLock(
                builder: (args) => SohoMonitoringApp(),
                lockScreen: AuthenticationScreen(),
                enabled: false,
                backgroundLockLatency: const Duration(seconds: 5),
          )
      )
  );
}
/*
class SohoMonitoringApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
        title: 'SoHo Network Monitor',
        //theme: appTheme(),
        initialRoute: '/',
        routes: routes,
    );
  }
}*/

class SohoMonitoringApp extends StatelessWidget {
  final String activationUrl="hawkeye.msenseiot.com";
  final GlobalKey webViewKey = GlobalKey();

  //InAppWebViewController? webViewController;
  final InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: true,
        javaScriptEnabled: true,
        javaScriptCanOpenWindowsAutomatically: true,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
        allowContentAccess: true,
        domStorageEnabled: true,
        saveFormData: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));
  Future <List<String>> getConnectedWiFi() async{
    // ignore: non_constant_identifier_names
    List<String> SSIDs=[];
    List<WifiNetwork> _wifiNetowrks = [];
    _wifiNetowrks = await WiFiForIoTPlugin.loadWifiList();
    for(var i=0;i<_wifiNetowrks.length;i++){
      SSIDs.add(_wifiNetowrks[i].ssid!);
    }
    String? _connectedSSID = await WiFiForIoTPlugin.getSSID();
    if (_connectedSSID !=null)
      {
        if (SSIDs.contains(_connectedSSID))
        {
          SSIDs.remove(_connectedSSID); // Remove the element before insterting at index 0
        }
        SSIDs.insert(0,_connectedSSID);
      }
    return SSIDs;
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'HawkEye',
        home: Scaffold(
            body:SafeArea(
                child: Column(children: <Widget>[
                  Expanded(
                    child: Stack(
                      children: [
                        InAppWebView(
                          key: webViewKey,
                          initialUrlRequest:
                          URLRequest(url: Uri.parse(activationUrl)),
                          initialOptions: options,
                          onWebViewCreated: (controller) async{
                            //webViewController = controller;
                            controller.addJavaScriptHandler(handlerName: "WiFi", callback: (args) {
                              print(args);
                              return  getConnectedWiFi();
                            });
                          },
                          onLoadStart: (controller, url) {
                          },
                          androidOnPermissionRequest: (controller, origin, resources) async {
                            print("androidOnPermissionRequest");
                            return PermissionRequestResponse(
                                resources: resources,
                                action: PermissionRequestResponseAction.GRANT);
                          },
                          shouldOverrideUrlLoading: (controller, navigationAction) async {
                            //var uri = navigationAction.request.url!;
                            print("shouldOverrideUrlLoading");
                            return NavigationActionPolicy.ALLOW;
                          },
                          onLoadStop: (controller, url) async {
                            print("onLoadStop");
                            print(url);
                          },
                          onLoadError: (controller, url, code, message) {
                            //print("Load erro");
                            print("onLoadError");
                          },
                          onProgressChanged: (controller, progress) {
                            //print(progress);
                          },
                          onUpdateVisitedHistory: (controller, url, androidIsReload) {

                          },
                          onConsoleMessage: (controller, consoleMessage) {
                            print(consoleMessage);
                          },
                        ),
                      ],
                    ),
                  ),
                  /* ButtonBar(
        alignment: MainAxisAlignment.center,
        children: <Widget>[
          ElevatedButton(
            child: Icon(Icons.arrow_back),
            onPressed: () {
              //webViewController?.goBack();
            },
          ),
          ElevatedButton(
            child: Icon(Icons.arrow_forward),
            onPressed: () {
              //webViewController?.goForward();
            },
          ),
          ElevatedButton(
            child: Icon(Icons.refresh),
            onPressed: () {
              //webViewController?.reload();
            },
          ),
        ],
      ),*/
                ]
                ))));
  }
}
