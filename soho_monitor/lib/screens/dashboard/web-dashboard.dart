// Copyright 2021, mSense Inc
// All rights reserved.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:soho_monitor/screens/appdrawer/drawer.dart';
import 'package:soho_monitor/screens/dashboard/components/body.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:io';

import 'components/navigation.dart';

// Display of Web Dashboard from platform

class WebDashboard extends StatefulWidget {
  const WebDashboard({Key? key, required this.activationUrl}) : super(key: key);
  final String activationUrl;
  @override
  _WebDashboardState createState() => _WebDashboardState();
}

class _WebDashboardState extends State<WebDashboard> {
  final _globalKey = GlobalKey<ScaffoldState>();
  final Completer<WebViewController> _controller = Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
        appBar: AppBar(
          //title: Text('My Devices',
          //  style: Theme.of(context).textTheme.headline6,),
          actions: <Widget>[
            NavigationControls(_controller.future),
          ],
        ),
        drawer: AppDrawerScreen(),
      body: Body(activationUrl: widget.activationUrl , controller:_controller, globalKey: _globalKey),
    );
  }



}
