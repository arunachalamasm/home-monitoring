// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/material.dart';
import 'package:soho_monitor/screens/devicediscovery/discoveryscreen.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NavigationControls extends StatelessWidget {
  final Future<WebViewController> _controllerFuture;

  const NavigationControls(this._controllerFuture, {key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _controllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        if (controller.hasData) {
          return Row(
            children: <Widget>[
              _buildAddBtn(context),
              _buildHistoryBackBtn(context, controller),
              _buildReloadBtn(controller),
              _buildHistoryForwardBtn(context, controller),
            ],
          );
        }

        return Container();
      },
    );
  }

  IconButton _buildReloadBtn(AsyncSnapshot<WebViewController> controller) {
    return IconButton(
      icon: Icon(Icons.refresh),
      onPressed: () {
        controller.data?.reload();
      },
    );
  }

  IconButton _buildAddBtn(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.add),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return DeviceDiscoveryScreen();
        })).then((value) => null);
      },
    );
  }

  IconButton _buildHistoryForwardBtn(
      BuildContext context, AsyncSnapshot<WebViewController> controller) {
    return IconButton(
      icon: Icon(Icons.arrow_forward_outlined),
      onPressed: () async {
        if (await controller.data?.canGoForward()?? true) {
        controller.data?.goForward();
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('No forward history', style: TextStyle(fontSize: 20),
              ),
            ),
          );
        }
      },
    );

  }

  IconButton _buildHistoryBackBtn(
      BuildContext context, AsyncSnapshot<WebViewController> controller) {
    return IconButton(
      icon: Icon(Icons.arrow_back_outlined),
      onPressed: () async {
        if (await controller.data?.canGoBack() ?? true) {
          controller.data?.goBack();
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(
                'No back history',
                style: TextStyle(fontSize: 20),
              ),
            ),
          );
        }
      },
    );
  }
}
