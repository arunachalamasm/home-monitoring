// Copyright 2021, mSense Inc
// All rights reserved.
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:soho_monitor/services/login/login.dart';
import 'package:webview_flutter/webview_flutter.dart';

// Main Content viewer. This needs to be updated with new HTTPS URL once platform is hosted on cloud
class Body extends StatelessWidget {
  Body({Key? key, required this.activationUrl, required this.controller, required this.globalKey}) : super(key: key);
  final String activationUrl;
  final Completer<WebViewController> controller;
  final globalKey;
  //final String _dashboardsurl = 'https://hawkeye.msenseiot.com/dashboard/c69ff170-eaf5-11eb-95d0-1fb6e2a0841a?publicId=31d338b0-d5c2-11eb-aff1-e52bf80600d4&deviceName=';
  Future<String> fetchToken(String username, String password) async{
    LoginClient loginClient = new LoginClient();
    String response = await loginClient.fetchToken(username,password);
    return response;
  }

  @override
  Widget build(BuildContext context)  {
    return Padding (
        padding:EdgeInsets.symmetric(vertical: 1),
        child:  WebView(
          //append deviceName(btid) at end of the _dashboardsurl
           //initialUrl: _dashboardsurl+btId,
          initialUrl: activationUrl,
          gestureNavigationEnabled: true,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            controller.complete(webViewController);
          },
          navigationDelegate: (request) {
            return _buildNavigationDecision(request);
          },
          javascriptChannels: <JavascriptChannel>[
            _createTopBarJsChannel(),
          ].toSet(),
          onPageFinished: (url) {
            _showPageTitle();
          },
        ));
  }

  NavigationDecision _buildNavigationDecision(NavigationRequest request) {
    /// if condition is dummy for reference to be used to preventing navigation if required in future based on url
    /*if (request.url.contains('login123')) {
      globalKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'You do not have rights to access My Account page',
            style: TextStyle(fontSize: 20),
          ),
        ),
      );

      return NavigationDecision.prevent;
    }*/

    return NavigationDecision.navigate;
  }

  void _showPageTitle() {
    controller.future.then((webViewController) {
      webViewController
          .evaluateJavascript('TopBarJsChannel.postMessage(document.title);');
    });
  }

  /// placeholder function to create communication channel with dashboard
  JavascriptChannel _createTopBarJsChannel() {
    return JavascriptChannel(
      name: 'TopBarJsChannel',
      onMessageReceived: (JavascriptMessage message) {
        String newTitle = message.message;
        if (newTitle.contains('-')) {
          newTitle = newTitle.substring(0, newTitle.indexOf('-')).trim();
        }
      },
    );
  }
}
