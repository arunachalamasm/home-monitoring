// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter/cupertino.dart';

class Body extends StatelessWidget {
  const Body({Key? key, this.state}) : super(key: key);

  final BluetoothState? state;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.bluetooth_disabled,
              size: 200.0,
              //color: Colors.red[900],
            ),
            Text(
              //'Bluetooth Adapter is ${state != null ? state.toString().substring(15) : 'not available'}.',
              'Bluetooth is Off',
              style: Theme.of(context).textTheme.subtitle2,
            ),
            Text(
              //'Bluetooth Adapter is ${state != null ? state.toString().substring(15) : 'not available'}.',
              'Enable Bluetooth to Proceed.',
              style: Theme.of(context).textTheme.caption,
              //textAlign: TextAlignVertical,
            ),
          ],
        ),
      );
  }
}
