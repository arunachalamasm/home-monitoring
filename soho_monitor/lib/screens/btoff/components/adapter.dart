// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter/cupertino.dart';

// Various Widgets used on FindMyScreen and DeviceScreen Classes
// These Widgets are:
//  - ScanResultTile for displaying Bluetooth Discovered Devices
//  - WifiDiscoveryTile for displaying WiFi networks on pi device
//  - PasswordField to take input for configuration of Wifi on pi 4 device. This is showed in alertDialog on selection of Wifi SSID
//  - AdapterStateTile is used to display error if Bluetooth is Off. This is displayed only if user is on FindMyScree.


class AdapterStateTile extends StatelessWidget {
  const AdapterStateTile({Key? key, required this.state}) : super(key: key);

  final BluetoothState state;

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.redAccent,
      child: ListTile(
        title: Text(
          'Bluetooth adapter is ${state.toString().substring(15)}',
          style: Theme.of(context).primaryTextTheme.subtitle1,
        ),
        trailing: Icon(
          Icons.error,
         // color: Theme.of(context).primaryTextTheme.subtitle1?.color,
        ),
      ),
    );
  }
}
