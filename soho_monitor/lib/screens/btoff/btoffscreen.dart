// Copyright 2021, mSense Inc
// All rights reserved.
import 'package:flutter/material.dart';
import 'package:soho_monitor/screens/btoff/components/body.dart';
import 'package:flutter_blue/flutter_blue.dart';
// Home screen after application launch

class BluetoothOffScreen extends StatefulWidget {
  const BluetoothOffScreen({Key? key, required this.state}) : super(key: key);
  final BluetoothState? state;
  @override
  _BluetoothOffScreenState createState() => _BluetoothOffScreenState();
}

class _BluetoothOffScreenState extends State<BluetoothOffScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Body(state: widget.state),
    );
  }
}
