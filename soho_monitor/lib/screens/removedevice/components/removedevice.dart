// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/widgets/deletedialog.dart';

class RemoveDeviceTile extends StatelessWidget {
  const RemoveDeviceTile({Key? key, required this.name, required this.btId, required this.notifyParent})
      : super(key: key);

  final String name;
  final String btId;
  final Function() notifyParent;

  Widget _buildTitle(BuildContext context) {
    if (name.length > 0 ) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 1.0),
              child: ListTile(
                title: Text(
                  name,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
            ),
          ),
        ],
      );

    } else {
      return Text(btId);
    }
  }

  String getNiceHexArray(List<int> bytes) {
    return '[${bytes.map((i) => i.toRadixString(16).padLeft(2, '0')).join(', ')}]'
        .toUpperCase();
  }

  String getNiceManufacturerData(Map<int, List<int>> data) {
    if (data.isEmpty) {
      return 'N/A';
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add(
          '${id.toRadixString(16).toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  String getNiceServiceData(Map<String, List<int>> data) {
    if (data.isEmpty) {
      return 'N/A';
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add('${id.toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: null,
        child:Card(
          //color: Colors.lightGreen,
            borderOnForeground: true,
            child:ListTile(
              title: _buildTitle(context),
              trailing: IconButton(
                //alignment: Alignment.centerRight,
                icon: Icon(
                  Icons.delete_rounded,
                  //color: Colors.indigo[900],
                  //size: 30,
                ),
                onPressed: ()  async {
                  // Delete all the entries related to this devices
                  RemoveDevice bForceRemove = await showDeleteDialog(context,btId);
                  if (bForceRemove != RemoveDevice.donotRemove) {
                    await showDeleteStatusDialog(context, btId, bForceRemove);
                    notifyParent();
                  }
                },
              ),
            )));
  }
}
