// Copyright 2021, mSense Inc
// All rights reserved.
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/deviceinfo.dart';
import 'package:soho_monitor/screens/removedevice/components/removedevice.dart';
import 'package:rxdart/rxdart.dart';

class Body extends StatefulWidget {
  @override
  _RemoveState createState() => _RemoveState();
}
class _RemoveState extends State<Body> {
  final List <String> _linkedDevices = [];
  final BehaviorSubject<List<DeviceInfo>> _btDevices = BehaviorSubject.seeded([]);
  Stream<List<DeviceInfo>> get btDevices => _btDevices.stream;
  // function to be used by child widgets to trigger update of the main screen
  void loadLinkedDevices()  {
    _linkedDevices.clear();
    _btDevices.add([]);
    List<String>? btDevicesList = AppSettings.instance.linkedDevices;
    if(btDevicesList != null){
      // create Device Info Object List add to bt devices stream to the list
      List<DeviceInfo> devices = [];
      btDevicesList.forEach((btDevice) {

        DeviceInfo device = DeviceInfo.fromJson(jsonDecode(btDevice));
        devices.add(device);
      });
      _btDevices.add(devices);
    }
    //FlutterBlue.instance.stopScan(); // Stop any existing scan before starting a new scan to avoid exceptions and no results
    //FlutterBlue.instance.startScan(timeout: Duration(seconds: 5));

  }

  void refreshScreen(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    loadLinkedDevices(); // get linked devices stream
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
              child:  Center(
                  child: Text("WARNING: Removing device will require reconfiguration. Wi-Fi connection removal can fail if mobile phone is not close to the device that is being removed.",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.red,
                        fontWeight: FontWeight.w600),
                  )
              ),
          ),
          StreamBuilder<List<DeviceInfo>>(
            stream: btDevices,
            initialData: [],
            builder: (c, snapshot)
            {
              if (snapshot.data!.isNotEmpty){
                return Column(
                  children: snapshot.data!
                      .map(
                        (r) {

                      if(r.name.length>0){

                        if (!_linkedDevices.contains(r.btId))
                          _linkedDevices.add(r.btId);
                        // Get Bluetooth Device corresponding to this btID and connect
                        //WiFiService.findBtDevice(r.btId).then((_device) => _device?.connect(autoConnect: false));
                        return RemoveDeviceTile(
                          name: r.name,
                          btId: r.btId,
                          notifyParent: refreshScreen,
                        );
                      }
                      else {
                        return Container(); // Return Empty Container if it is not a Hawkeye Device
                      }
                    },
                  )
                      .toList(),
                );
              } else return Container(
                child: Text("No linked device found!!"),
              );
            },
          ),
        ],
      ),
    );
  }
 }

