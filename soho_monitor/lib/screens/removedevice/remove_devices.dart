// Copyright 2021, mSense Inc
// All rights reserved.
import 'package:flutter/material.dart';
import './components/body.dart';
// Home screen after application launch

class RemoveDevicesScreen extends StatefulWidget {
  @override
  _RemoveDevicesScreenState createState() => _RemoveDevicesScreenState();
}

class _RemoveDevicesScreenState extends State<RemoveDevicesScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Remove Devices',
          style: Theme.of(context).textTheme.headline6,),
      ),
        body: Body(),
    );
  }
}
