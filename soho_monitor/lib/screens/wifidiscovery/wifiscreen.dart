// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:soho_monitor/screens/wifidiscovery//components/body.dart';
import 'package:soho_monitor/services/wifi/wifi_service.dart';
import 'package:soho_monitor/services/wifi/wifi_services.dart';

// Home screen after application launch

class WifiDiscoveryScreen extends StatefulWidget {
  const WifiDiscoveryScreen({Key? key, required this.btDevice}) : super(key: key);
  final BluetoothDevice btDevice;

  @override
  _WifiDiscoveryScreenState createState() => _WifiDiscoveryScreenState();
}

class _WifiDiscoveryScreenState extends State<WifiDiscoveryScreen> {

  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new FutureBuilder(
        future:  WifiServices.instance.getService(widget.btDevice.id.toString()),
        builder: (BuildContext context, AsyncSnapshot<WiFiService?> snapshot) {
          if (snapshot.hasData){
          WiFiService? _wifiService = snapshot.data;
          _wifiService?.startWiFiScan(Duration(seconds: 60));
          return Scaffold(
            appBar: AppBar(
              /*title: Text(device.name),*/
              title: Text("Wi-Fi",
                style: Theme.of(context).textTheme.headline6,),
              leading: IconButton(icon: Icon(Icons.arrow_back_rounded),onPressed: (){
                //wifiService.disconnect();
                Navigator.pop(context);
              },),

            ),
            //drawer: AppDrawerScreen(),
            floatingActionButton: StreamBuilder<bool>(
              stream: _wifiService?.isScanning,
              initialData: false,
              builder: (c, snapshot) {
                if (snapshot.data!) {
                  return FloatingActionButton(
                    child: Icon(Icons.stop),
                    onPressed: () {
                      _wifiService?.stopWiFiScan();
                    },
                    backgroundColor: Colors.red,
                  );
                } else {
                  return FloatingActionButton(
                      child: Icon(Icons.refresh),
                      onPressed: ()  {
                        _wifiService?.startWiFiScan(Duration(seconds: 60));
                      });
                }
              },
            ),
            body: Body(wifiService: _wifiService),
          );
          }
          else{
            return Scaffold(
              appBar: AppBar(
                /*title: Text(device.name),*/
                title: Text("Wi-Fi",
                  style: Theme.of(context).textTheme.headline6,),
                leading: IconButton(icon: Icon(Icons.arrow_back_rounded),onPressed: (){
                  //wifiService.disconnect();
                  Navigator.pop(context);
                },),
              ),
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    CupertinoActivityIndicator(
                      radius: 20, animating: true,),
                    Text("Connecting to HawkEye Device...",
                      style: Theme.of(context).textTheme.subtitle2,),
                  ],
                ),
              ),
            );
          }
        });
  }
}
