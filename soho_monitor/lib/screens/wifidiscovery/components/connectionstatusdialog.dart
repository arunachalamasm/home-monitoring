// Copyright 2021, mSense Inc
// All rights reserved.


import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/models/deviceinfo.dart';
import 'package:soho_monitor/screens/dashboard/web-dashboard.dart';
import 'package:soho_monitor/services/dcs/dcs.dart';
import 'package:soho_monitor/services/ucs/ucs.dart';
import 'package:soho_monitor/services/wifi/wifi_service.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';

Future <bool> showConnectionStatusDialog(BuildContext context, String ssid, WiFiService? wifiService) async {
  bool ret =false;
  bool autoCloseDialog = false;
  StateSetter? updateStatus;
  String statusText = "Configuring WiFi...";
  String buttonText = "Cancel";
  bool deviceLinked =  false;
  String activationUrl = "";
  Future <DeviceLinkingStatus> linkDevice(String btId) async{
    // Add Information to app settings
    String? name = AppSettings.instance.getDeviceName(btId);
    if(name == null ){
      name = AppSettings.instance.getUniqueName(btId);
    }

    DCSClient  dcsClient = new DCSClient();
    DeviceCreationResponse response = await dcsClient.add(btId,AppSettings.instance.userId);
    if (response == DeviceCreationResponse.success) {
      AppSettings.instance.setDeviceName(btId, name);
    }
    else if (response == DeviceCreationResponse.deviceExists) {
      print('Device already exists.');
      return DeviceLinkingStatus.deviceExists;
    }
    else if (response == DeviceCreationResponse.networkError) {
      print('Network Error.');
      return DeviceLinkingStatus.networkError;
    }

    List <String>? btDevices =  AppSettings.instance.linkedDevices;
    DeviceInfo device = new DeviceInfo(name: name,btId: btId);
    String deviceJson = jsonEncode(device);
    if(btDevices == null){
      // add the bt device to the list
      btDevices = [deviceJson];
    }
    else{
      // Add if this does not exist
      bool bExists = false;
      btDevices.forEach((btDevice) {
        DeviceInfo device = DeviceInfo.fromJson(jsonDecode(btDevice));
        if (device.btId == btId)
        {
          bExists = true;
        }
      });
      if(!bExists)
        btDevices.add(deviceJson);
    }
    AppSettings.instance.setLinkedDevices(btDevices);
    return DeviceLinkingStatus.success;
  }

  Timer _statusTimer = Timer.periodic(Duration(seconds: 1), (Timer timer) async {
    WirelessStatus? status;
    status = await wifiService?.getWiFiConnectionStatus();
    updateStatus!(() {
      if(status != null && status != WirelessStatus.unknown){
        statusText= wirelessStatusString[status.index];
        if(status == WirelessStatus.needAuth)
          {
            statusText= "Connection failed due to invalid credentials. Try Again!!";
            buttonText = "OK";
            timer.cancel();
          }
      }
    });

    if(status == WirelessStatus.activated) {
      timer.cancel();
      if(wifiService !=null){
        DeviceLinkingStatus _status = await linkDevice(wifiService.getBtDeviceInfo().id.toString());
        if(_status == DeviceLinkingStatus.success) {
          deviceLinked = true;
          //autoCloseDialog = true;
          statusText= "Device linked successfully!!";
          UCSClient  ucsClient = new UCSClient();
          UserCreationResponse response = await ucsClient.add(AppSettings.instance.userId);
          if (response == UserCreationResponse.userExists) {
            activationUrl = ucsClient.getActivationURL();
            autoCloseDialog = true;
          }

        }
        else if(_status == DeviceLinkingStatus.deviceExists) {
            statusText= "This HawkEye device is already linked with another user. Please contact device provider!!";
            buttonText = "OK";
          }
        else if(_status == DeviceLinkingStatus.networkError) {
          wifiService.disconnectWifi(); // Disconnect Wifi
          statusText= "Please check your internet connection and try again!!";
          buttonText = "OK";
        }
      }
    }
    /*updateStatus!(() {
    });*/

  });
  ret = await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (context) {
      return StatefulBuilder(builder: (context, StateSetter setState){
        updateStatus = setState;
        if(autoCloseDialog){
	        //Navigator.of(context).pop(deviceLinked);
          _statusTimer.cancel();  // cancel the timer to ensure it gets canceled here if not done earlier
         return WebDashboard(activationUrl: activationUrl);

        }
        return AlertDialog(
            title: Text(ssid,
            style: Theme.of(context).textTheme.subtitle1,),
            content:  Text(statusText + "...",
            style: Theme.of(context).textTheme.caption,),

          actions: <Widget>[
            new TextButton(
              child: new Text(buttonText),
                        onPressed: () {
                          _statusTimer.cancel();
                          Navigator.of(context).pop(deviceLinked);
                        },
                ),
              ],
              );
      });
    },
  );
 _statusTimer.cancel();  // cancel the timer to ensure it gets canceled here if not done earlier
 return ret;
}





