// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/material.dart';
import 'package:soho_monitor/services/wifi/wifi_service.dart';
import 'package:soho_monitor/screens/wifidiscovery/components/wifidiscovery.dart';
import 'package:soho_monitor/models/access_point.dart';
import 'package:flutter/cupertino.dart';


class Body extends StatefulWidget {
  const Body({Key? key, this.wifiService}) : super(key: key);
  final WiFiService? wifiService;

  @override
  _WiFiDiscoveryState createState() => _WiFiDiscoveryState();
}

class _WiFiDiscoveryState extends State<Body> {
  void reloadNetworks(bool scanWifi){
    if(scanWifi)
      widget.wifiService?.startWiFiScan(Duration(seconds: 30));
    else{
      /*setState(() {
        //Refresh page
      });*/
      Navigator.of(context).pop();
    }
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          StreamBuilder<List <WifiAccessPoint>>(
            stream: widget.wifiService?.accessPoints,
            initialData: [],
            builder: (c, snapshot){
              if (snapshot.data!.isNotEmpty && widget.wifiService != null) {
                return Column(
                  children: snapshot.data!
                      .map((d) => WiFiDiscoveryTile(
                      accessPoint: d, wifiService: widget.wifiService, notifyParent: reloadNetworks))
                      .toList(),
                );
              }
              else{
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      CupertinoActivityIndicator(
                        radius: 20, animating: true,),
                      Text("Fetching Wi-Fi Networks...",
                        style: Theme.of(context).textTheme.subtitle2,),
                    ],
                  ),
                );
              }
            },
          ),
        ],
      ),
    );
  }
}

