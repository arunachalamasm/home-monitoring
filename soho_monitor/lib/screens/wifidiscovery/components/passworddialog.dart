// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:soho_monitor/services/wifi/wifi_service.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:soho_monitor/widgets/password.dart';

import 'connectionstatusdialog.dart';

Future <bool> showPasswordDialog(BuildContext context, String ssid, WiFiService? wifiService) async {
  final textController = TextEditingController();
  bool ret =false;
  ret = await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(ssid,
          style: Theme.of(context).textTheme.subtitle1,),
        content: PasswordTextField(
          controller: textController,
        ),
        actions: <Widget>[
          new TextButton(
            child: new Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          new TextButton(
            child: Text("Connect"),
            onPressed: () async {
              //await updateLinkedDevice(btId,textController.text);
              wifiService?.configureWifi(ssid,textController.text);
              ret = await showConnectionStatusDialog(context,ssid,wifiService);
              Navigator.of(context).pop(ret);
            },
          ),
        ],
      );
    },
  );
 return ret;
}
