// Copyright 2021, mSense Inc
// All rights reserved.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/services/dcs/dcs.dart';
import 'package:soho_monitor/services/wifi/wifi_service.dart';
import 'package:soho_monitor/models/access_point.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/models/deviceinfo.dart';
import 'package:soho_monitor/screens/wifidiscovery/components/passworddialog.dart';

import 'connectionstatusdialog.dart';

class WiFiDiscoveryTile extends StatefulWidget {
  final WifiAccessPoint accessPoint;
  final WiFiService? wifiService;
  final Function(bool) notifyParent;
  const WiFiDiscoveryTile({Key? key, required this.accessPoint, this.wifiService , required this.notifyParent})
      : super(key: key);

  @override
  _WiFiDiscoveryTileState createState() => _WiFiDiscoveryTileState();
}

class _WiFiDiscoveryTileState extends State<WiFiDiscoveryTile> {
  final controller = TextEditingController();
  void linkDevice(String mac, String btId) async{
    // Add Information to app settings
    String? name = AppSettings.instance.getDeviceName(btId);
    if(name == null ){
      DCSClient  dcsClient = new DCSClient();
      DeviceCreationResponse response = await dcsClient.add(btId,AppSettings.instance.userId);
      if (response == DeviceCreationResponse.deviceExists) {
        print('Device already exists.');
        return;
        //Future.error(StateError('Device is already exist'));
      }
      name = AppSettings.instance.getUniqueName(btId);
      AppSettings.instance.setDeviceName(btId, name);
    }

    List <String>? btDevices =  AppSettings.instance.linkedDevices;
    DeviceInfo device = new DeviceInfo(name: name,btId: btId);
    String deviceJson = jsonEncode(device);
    if(btDevices == null){
      // add the bt device to the list
      btDevices = [deviceJson];
    }
    else{
      // Add if this does not exist
      bool bExists = false;
      btDevices.forEach((btDevice) {
        DeviceInfo device = DeviceInfo.fromJson(jsonDecode(btDevice));
        if (device.btId == btId)
        {
          bExists = true;
        }
      });
      if(!bExists)
        btDevices.add(deviceJson);
    }
    AppSettings.instance.setLinkedDevices(btDevices);
  }

  Widget _buildIcons(BuildContext context, int signalStrength, bool protected) {
    if (protected){
      if(widget.accessPoint.hostAddress.isNotEmpty)
        return Icon(Icons.perm_scan_wifi_sharp);
      else
        return Icon(Icons.signal_wifi_4_bar_lock);
    }
    else{
      if(widget.accessPoint.hostAddress.isNotEmpty)
        return Icon(Icons.perm_scan_wifi_sharp);
      else
        return Icon(Icons.signal_wifi_4_bar);
    }
  }

  Widget _buildAdvRow(BuildContext context, String title, String value) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title, style: Theme.of(context).textTheme.caption),
          SizedBox(
            width: 12.0,
          ),
          Expanded(
            child: Text(
              value,
              style: Theme.of(context).textTheme.subtitle2,
              softWrap: true,
            ),
          ),
        ],
      ),
    );
  }
  Widget _buildTile(BuildContext context, WifiAccessPoint accessPoint, WiFiService? wifiService) {
    if (accessPoint.hostAddress.isNotEmpty && wifiService != null){
      //linkDevice(accessPoint.mac, wifiService.getBtDeviceInfo().id.toString()); // Link this device since connection is successful
      // Get the relative value of Signal Strength
      String signalStrength = accessPoint.signalStrength > 80 ? "Excellent": accessPoint.signalStrength > 40 ? "Good" :"Poor";
      return Card(
        //color: Colors.lightGreen,
        child: ExpansionTile(
          leading: _buildIcons(context, accessPoint.signalStrength,
              accessPoint.protected),
          trailing: SizedBox.shrink(),
          initiallyExpanded: true,
          /*trailing: IconButton(
              alignment: Alignment.topRight,
              icon: Icon(
                Icons.info_outline_rounded,
                //color: Colors.indigo[900],
                size: 30,
              ),
              onPressed: ()  {

            },
            ),*/
          title: Text(
            accessPoint.ssid,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          children: <Widget>[
            _buildAdvRow(
                context, 'Host Address :', accessPoint.hostAddress),
            _buildAdvRow(context, 'MAC Address :',
                accessPoint.mac),
            _buildAdvRow(context, 'Signal Strength :',signalStrength),
            TextButton(
              child: Text("Disconnect"),
              onPressed: () async {
                //Disconnect Wifi
                wifiService.disconnectWifi();
                widget.notifyParent(true); // We do not want to navigate to home page
              },
            ),
          ],
        ),
      );
    }
    else{
      return GestureDetector(
        onTap: () async{
          bool ret = await showPasswordDialog(context,accessPoint.ssid,wifiService);

          if(ret) {
            //ret = await showConnectionStatusDialog(context,accessPoint.ssid,wifiService);
            widget.notifyParent(!ret);
          }
          print(" Connection Request Initiated: $ret");
          //showPasswordDialog(context: context, ssid: accessPoint.ssid, wifiService: wifiService);
          /*Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => PasswordScreen(ssid: accessPoint.ssid,wifiService: wifiService)),
          );*/
        },
        child: Card(

            child: ListTile(
              leading: _buildIcons(context, accessPoint.signalStrength,
                  accessPoint.protected),
              /*trailing:  Icon(
                  Icons.arrow_forward_ios_sharp,
                  size: 30.0,
                  //color: Colors.indigo[900],
                ),*/
              title: Text(
                accessPoint.ssid,
                style: Theme.of(context).textTheme.subtitle1,
              ),
            )),
      );

    }
  }

  @override
  Widget build(BuildContext context) {
    if(widget.wifiService != null)
      return _buildTile(context, widget.accessPoint, widget.wifiService);
    else
      return Container();
  }
}
