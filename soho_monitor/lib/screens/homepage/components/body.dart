// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/screens/devicediscovery/discoveryscreen.dart';
import 'package:soho_monitor/screens/btoff/btoffscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/screens/dashboard/web-dashboard.dart';

// Content of application screens. It contains BT Discovery screen and WiFi Discovery Screen
// Bluetooth Discovery screen is handled by FindDevicesScreen
// Wifi Discovery is handled by DeviceScreen Class
// Base Code has been derived from example in flutter_blue

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //color: Colors.lightBlue,
      theme: ThemeData.from(colorScheme: ColorScheme.dark()),
      darkTheme: ThemeData.from(colorScheme: ColorScheme.dark()),
      themeMode: ThemeMode.dark,
      home: StreamBuilder<BluetoothState>(
          stream: FlutterBlue.instance.state,
          initialData: BluetoothState.unknown,
          builder: (c, snapshot) {
            final state = snapshot.data;
            if (state == BluetoothState.on) {
              if (AppSettings.instance.userId != null){
                String activationUrl = serverUrl;
                  return WebDashboard(activationUrl: activationUrl);
              }
              else{
                return DeviceDiscoveryScreen();
              }
            }
            return BluetoothOffScreen(state: state);
          }),
    );
  }
}



