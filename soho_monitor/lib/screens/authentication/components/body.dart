// Copyright 2021, mSense Inc
// All rights reserved.
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/screens/authentication/components/passcode_screen.dart';
import 'package:soho_monitor/screens/authentication/components/circle.dart';
import 'package:soho_monitor/screens/authentication/components/keyboard.dart';
import 'package:soho_monitor/widgets/app_lock.dart';
import 'package:soho_monitor/widgets/app_restart.dart';
class Body extends StatefulWidget {
  @override
  _AuthState createState() => _AuthState();
}
class _AuthState extends State<Body> {
  bool isFingerprint = false;
  final StreamController<bool> _verificationNotifier =
  StreamController<bool>.broadcast();

  bool isAuthenticated = false;

  AndroidAuthMessages _authMessages = const AndroidAuthMessages(
      cancelButton: 'Use PIN',
      goToSettingsButton: 'Settings',
      goToSettingsDescription: 'Please set up your Fingerprint!',
      biometricHint: ' ',
      signInTitle: 'Authentication');

  Future<Null> biometrics() async {
    final LocalAuthentication auth = new LocalAuthentication();
    bool authenticated = false;

    try {
      authenticated = await auth.authenticate(
          localizedReason: 'Scan registered finger to access HawkEye App',
          useErrorDialogs: true,
          androidAuthStrings: _authMessages,
          stickyAuth: true);
    } on PlatformException catch (e) {
      print(e);
      auth.stopAuthentication();
    }
    if (!mounted) return;
    if (authenticated) {
      setState(() {
        isFingerprint = true;
      });
    }
  }
  _onPasscodeEntered(String enteredPasscode) async{
    String? storedPasscode =  AppSettings.instance.passCode;
    if (storedPasscode == null)
      storedPasscode= "000000";  // Set Default is pin not present
    bool isValid = storedPasscode == enteredPasscode;
    _verificationNotifier.add(isValid);
    if (isValid) {
      setState(() {
        this.isAuthenticated = isValid;
      });
      AppLock.of(context)?.didUnlock('Unlock Successful');
    }
  }

  _onPasscodeCancelled() {
    Navigator.maybePop(context);
  }
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
  _buildPasscodeRestoreButton() => Align(
    alignment: Alignment.bottomLeft,
    child: Container(
      margin: const EdgeInsets.only(bottom: 5.0),
      child: TextButton(
        child: Text(
          "Reset PIN",
          textAlign: TextAlign.center,
          style: const TextStyle(
              fontSize: 12,
              //color: Colors.white,
              fontWeight: FontWeight.w300),
        ),
        onPressed: _resetAppPassword,
        // splashColor: Colors.white.withOpacity(0.4),
        // highlightColor: Colors.white.withOpacity(0.2),
        // ),
      ),
    ),
  );

  _resetAppPassword() {
    Navigator.maybePop(context).then((result) {
      if (!result) {
        return;
      }
      _showResetPinDialog(() async {
        Navigator.maybePop(context);
        //TODO: Clear all application data here
        AppSettings.instance.resetPassCode();
        RestartApp.restartApp(context);

      });
    });
  }
  _showResetPinDialog(VoidCallback onAccepted) {
    showDialog(
      context: context,
      barrierDismissible: false,  // Modal Dialog
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.grey[800],
          title: Text(
            "Reset PIN",
            style: const TextStyle(
                fontSize: 18,
                color: Colors.white,
               ),

          ),
          content: Text(
            "This action will remove all stored application user data for security reasons. Are you sure you want to Reset PIN?",
            style: const TextStyle(
                fontSize: 12,
                color: Colors.white,
                ),
          ),
          actions: <Widget>[
            new TextButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            new TextButton(
              child: Text("OK"),
              onPressed: onAccepted,
            ),
          ],
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {

    return PasscodeScreen(
      title: Text(
        'Enter PIN',
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white, fontSize: 28),
      ),
      circleUIConfig: CircleUIConfig(
          borderColor: Colors.grey,
          fillColor: Colors.blue,
          circleSize: 30),
      keyboardUIConfig: KeyboardUIConfig(
          digitBorderWidth: 2, primaryColor: Colors.grey),

      passwordEnteredCallback: _onPasscodeEntered,
      deleteButton: Icon(
        Icons.arrow_back,
        color: Colors.white,
        size: 40,
      ),
      shouldTriggerVerification: _verificationNotifier.stream,
      backgroundColor: Colors.black.withOpacity(0.9),
      cancelCallback: _onPasscodeCancelled,
      passwordDigits: 6,
      bottomWidget: _buildPasscodeRestoreButton(),
    );

    /*return LockScreen(
        title: "Enter PIN ",
        passLength: defaultPin.length,
        bgImage: "images/bg.png",
        fingerPrintImage: Image.asset(
          "images/fingerprint.png",
          height: 40,
          width: 40,
        ),
        showFingerPass: _enableFingerprint,
        fingerFunction: biometrics,
        numColor: Colors.blue,
        fingerVerify: isFingerprint,
        borderColor: Colors.white,
        showWrongPassDialog: true,
        wrongPassContent: "Wrong PIN. Try Again!",
        wrongPassTitle: "Wrong PIN",
        wrongPassCancelButtonText: "Cancel",
        passCodeVerify: (pin) async {
          for (int i = 0; i < defaultPin.length; i++) {
            if (pin[i] != defaultPin[i]) {
              return false;
            }
          }
          return true;
        },
        onSuccess: () {
          AppLock.of(context)?.didUnlock('Unlock Successful');
        });*/
  }
}
