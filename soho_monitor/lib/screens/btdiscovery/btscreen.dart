// Copyright 2021, mSense Inc
// All rights reserved.
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:soho_monitor/screens/appdrawer/drawer.dart';
import 'package:soho_monitor/screens/btdiscovery//components/body.dart';

// Home screen after application launch

class DeviceDiscoveryScreen extends StatefulWidget {

  @override
  _DeviceDiscoveryScreenState createState() => _DeviceDiscoveryScreenState();
}

class _DeviceDiscoveryScreenState extends State<DeviceDiscoveryScreen> {
  @override
  void initState() {
    super.initState();
    // Start Scan after init
    FlutterBlue.instance.startScan(timeout: Duration(seconds: 5));
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {

    void reload()  {
      // Notify Widget tp reload
      FlutterBlue.instance.startScan(timeout: Duration(seconds: 5));
      setState(() {
      });
      print(" Reload called for state update");
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('My Devices',
          style: Theme.of(context).textTheme.headline6,),
      ),
      drawer: AppDrawerScreen(),
      floatingActionButton: StreamBuilder<bool>(
        stream: FlutterBlue.instance.isScanning,
        initialData: false,
        builder: (c, snapshot) {
          if (snapshot.data!) {
            return FloatingActionButton(
              child: Icon(Icons.stop),
              onPressed: () => FlutterBlue.instance.stopScan(),
              backgroundColor: Colors.red,
            );
          } else {
            return FloatingActionButton(
                child: Icon(Icons.search),
                onPressed: () async {
                  reload();
                });
          }
        },
      ),
        body: Body(notifyParent: reload,),
    );
  }
}
