// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/models/access_point.dart';
import 'dart:async';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/services/wifi/wifi_service.dart';
import 'package:soho_monitor/services/wifi/wifi_services.dart';

Future <WifiAccessPoint?> getDeviceInfo(String btId) async{
    WiFiService? _wifiService = await WifiServices.instance.getService(btId);
    if(_wifiService != null){
      WifiAccessPoint? ap = await _wifiService.getCurrentWiFiConnection();
      return ap;
    }
}

/*Widget _buildAdvRow(BuildContext context, String title, String value) {
  return Column(
      children: <Widget>[
        Text(title, style: Theme.of(context).textTheme.caption),
        Text(
          value,
          style: Theme.of(context).textTheme.subtitle2,
          softWrap: true,
        ),
      ],
    );
}*/
/*Widget _buildAdvRow(BuildContext context, String title, String value) {
  return Card(
    child:
      Text(
        value,
        style: Theme.of(context).textTheme.subtitle2,
        softWrap: true,
      ),
  );
}*/

Widget _buildAdvRow(BuildContext context, String title, String value) {
  return Padding(
    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 4.0),
    child: Column(
      children: <Widget>[
        Text(title, style: Theme.of(context).textTheme.caption),
        SizedBox(
          width: 20.0,
        ),
       Text(
            value,
            style: Theme.of(context).textTheme.bodyText2,
            softWrap: true,
          ),

      ],
    ),
  );
}
Future <void> showDeviceInfoDialog(BuildContext context, String btId, WifiAccessPoint? accessPoint) async {
  String? deviceName = AppSettings.instance.getDeviceName(btId);
  String title = ((deviceName!=null)?deviceName:"Device") + " Info";
  // Get the relative value of Signal Strength
  if (accessPoint != null){
    String signalStrength = accessPoint.signalStrength > 80 ? "Excellent": accessPoint.signalStrength > 40 ? "Good" :"Poor";
    await showDialog(
      context: context,
      barrierDismissible: false,  // Modal Dialog
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title,
            style: Theme.of(context).textTheme.headline5,),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          content: Container(
            child:ListView(
              shrinkWrap: true,
              children: <Widget>[
                _buildAdvRow(
                    context, 'SSID :', accessPoint.ssid),
                _buildAdvRow(
                    context, 'IP Address :', accessPoint.hostAddress),
                _buildAdvRow(context, 'Signal Strength :',signalStrength),
                _buildAdvRow(context, 'Bluetooth ID :',btId),
            ],
          )
          ),
          actions: <Widget>[
            new TextButton(
              child: Text("OK"),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  else{

    await showDialog(
      context: context,
      barrierDismissible: false,  // Modal Dialog
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title,
            style: Theme.of(context).textTheme.subtitle1,),
          content: Text("Device info could not be retrieved. Please ensure you are close to the device and try again.",
            style: Theme.of(context).textTheme.caption,),
          actions: <Widget>[
            new TextButton(
              child: Text("OK"),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
