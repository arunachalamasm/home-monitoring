// Copyright 2021, mSense Inc
// All rights reserved.

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/models/access_point.dart';
import 'package:soho_monitor/screens/btdiscovery/components/editdialog.dart';

import 'deviceinfodialog.dart';

class LinkedDeviceTile extends StatelessWidget {
  const LinkedDeviceTile({Key? key, required this.name, required this.btId, this.onTap, required this.notifyParent})
      : super(key: key);

  final String name;
  final String btId;
  final VoidCallback? onTap;
  final Function() notifyParent;

  Widget _buildTitle(BuildContext context) {
    if (name.length > 0 ) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 1.0),
              child: ListTile(
                title: Text(
                  name,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
            ),
          ),
        ],
      );

    } else {
      return Text(btId);
    }
  }

  String getNiceHexArray(List<int> bytes) {
    return '[${bytes.map((i) => i.toRadixString(16).padLeft(2, '0')).join(', ')}]'
        .toUpperCase();
  }

  String getNiceManufacturerData(Map<int, List<int>> data) {
    if (data.isEmpty) {
      return 'N/A';
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add(
          '${id.toRadixString(16).toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  String getNiceServiceData(Map<String, List<int>> data) {
    if (data.isEmpty) {
      return 'N/A';
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add('${id.toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child:Card(
          //color: Colors.lightGreen,
            borderOnForeground: true,
            child:ListTile(
              title: _buildTitle(context),
              trailing: IconButton(
                //alignment: Alignment.centerLeft,
                icon: Icon(
                  Icons.edit_outlined,
                  //color: Colors.indigo[900],
                  // size: 30,
                ),
                onPressed: ()  async {
                  // Edit Name of Device
                  bool refresh = await showEditDialog(context,btId,name);
                  if(refresh) notifyParent();
                },
              ),
              /// information icon to show if hawkeye device is connected to platform and it's details
              leading: IconButton(
                  icon: Icon(
                    Icons.info,
                    //color: Colors.green,
                    color: Colors.white,
                    size: 35,
                  ),
                  onPressed: ()  async {
                    // Add Connected info here
                    WifiAccessPoint? _accessPoint = await getDeviceInfo(btId);
                    showDeviceInfoDialog(context,btId,_accessPoint);
                  },),
            )));
  }
}
