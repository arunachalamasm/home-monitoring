// Copyright 2021, mSense Inc
// All rights reserved.
import 'package:flutter/material.dart';
import 'package:soho_monitor/screens/appdrawer/components/body.dart';
// Home screen after application launch

class AppDrawerScreen extends StatefulWidget {
  @override
  _AppDrawerScreenState createState() => _AppDrawerScreenState();
}

class _AppDrawerScreenState extends State<AppDrawerScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Body(),
    );
  }
}
