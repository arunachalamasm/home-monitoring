// Copyright 2021, mSense Inc
// All rights reserved.
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/screens/pinchange/pin.dart';
import 'package:package_info/package_info.dart';
import 'package:soho_monitor/screens/removedevice/remove_devices.dart';
import 'package:soho_monitor/widgets/app_restart.dart';
class Body extends StatefulWidget {
  @override
  _DrawerState createState() => _DrawerState();
}
class _DrawerState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    bool _pinSet = true;
    bool _linkedDevicesExist = true;
    String? _userEmail = AppSettings.instance.userId;
    String _userId = "User not signed in.";
    if(_userEmail != null)
      _userId = _userEmail;

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      //String appName = packageInfo.appName;
      //String packageName = packageInfo.packageName;
      //String buildNumber = packageInfo.buildNumber;
      setState(() {
      });
    });
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(tenantName),
            //accountEmail: Text(_version,style: Theme.of(context).textTheme.caption,),
            accountEmail: Text(_userId,style: Theme.of(context).textTheme.caption,),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.indigo[900],
              child: Icon(Icons.visibility),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home), title: Text("Home"),
            onTap: () {
              Navigator.pop(context);
              //Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen()));
            },
          ),
          if (_pinSet)
            ListTile(
              leading: Icon(Icons.lock), title: Text("Change PIN"),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (_) => PinConfigurationScreen()));
              },

            ),
          if(_linkedDevicesExist) // show only is there is a linked device
            ListTile(
              leading: Icon(Icons.delete), title: Text("Remove Device"),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (_) => RemoveDevicesScreen()));
              },
            ),
          if(_userEmail !=null) // sign out option
            ListTile(
              leading: Icon(Icons.logout), title: Text("Sign Out "),
              onTap: () {
                Navigator.pop(context);
                //TODO: Clear all application data here
                AppSettings.instance.removeUser();
                RestartApp.restartApp(context);
              },
            )
          else // show sign option
            ListTile(
              leading: Icon(Icons.login), title: Text("Sign In /Sign Up"),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (_) => RemoveDevicesScreen()));
              },
            ),

        ],
      ),
    );
  }
 }

