// Copyright 2021, mSense Inc
// All rights reserved.
import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_mdns_plugin/flutter_mdns_plugin.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/screens/appdrawer/drawer.dart';
import 'package:soho_monitor/screens/devicediscovery/components/btdiscovery.dart';
import 'package:soho_monitor/screens/devicediscovery/components/zeroconfdiscovery.dart';

// Home screen after application launch

class DeviceDiscoveryScreen extends StatefulWidget {

  @override
  _DeviceDiscoveryScreenState createState() => _DeviceDiscoveryScreenState();
}

class _DeviceDiscoveryScreenState extends State<DeviceDiscoveryScreen> {
  String _hawkeye_discovery_service = "_workstation._tcp";
  late FlutterMdnsPlugin _mdnsPlugin;
  List<String> messageLog = <String>[];
  late DiscoveryCallbacks discoveryCallbacks;
  bool isScanningNetwork = false;
  LinkedHashMap _discoveredServices = new LinkedHashMap<String, ServiceInfo>();  // For Tracking discovered devices
  @override
  void initState() {
    super.initState();
    if (useZeroConfDiscovery){
      discoveryCallbacks = new DiscoveryCallbacks(
        onDiscovered: (ServiceInfo info) {

          print("Discovered ${info.toString()}");
          /*setState(() {
            messageLog.insert(0, "DISCOVERY: Discovered ${info.toString()}");
          });*/
        },
        onDiscoveryStarted: () {
          print("Discovery started");
          isScanningNetwork = true;
          /*setState(() {
            messageLog.insert(0, "DISCOVERY: Discovery Running");
          });*/
        },
        onDiscoveryStopped: () {
          print("Discovery stopped");
          isScanningNetwork = false;
          setState(() {
            messageLog.insert(0, "DISCOVERY: Discovery Not Running");
          });
        },
        onResolved: (ServiceInfo info) {
          _discoveredServices.putIfAbsent(info.name, () => info);
          print("Resolved Service ${info.toString()}");
          print("Discovered Services ${_discoveredServices.toString()}");
          /*setState(() {
            messageLog.insert(0, "DISCOVERY: Resolved ${info.toString()}");
          });*/
        },
      );
      //messageLog.add("Starting mDNS for service [$_hawkeye_discovery_service]");
      _mdnsPlugin = new FlutterMdnsPlugin(discoveryCallbacks: discoveryCallbacks);
      _mdnsPlugin.startDiscovery(_hawkeye_discovery_service);
      isScanningNetwork = true;
      Timer(Duration(seconds: 5), () => _mdnsPlugin.stopDiscovery());
    }
    else {
      // Start Scan after init
      FlutterBlue.instance.startScan(timeout: Duration(seconds: 5));
    }
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {

    void reload()  {

      if (useZeroConfDiscovery){
        _mdnsPlugin.startDiscovery(_hawkeye_discovery_service);
        isScanningNetwork = true;
        Timer(Duration(seconds: 5), () => _mdnsPlugin.stopDiscovery());
      }
      else {
        // Notify Widget tp reload
        FlutterBlue.instance.startScan(timeout: Duration(seconds: 5));
      }
      setState(() {
      });
      print(" Reload called for state update");
    }

    Widget? floatingButton(){
      if (useZeroConfDiscovery){
        if (isScanningNetwork){
          return FloatingActionButton(
            child: Icon(Icons.stop),
            onPressed: () => _mdnsPlugin.stopDiscovery(),
            backgroundColor: Colors.red,
          );
        }
        else{
          return FloatingActionButton(
              child: Icon(Icons.search),
              onPressed: () async {
                reload();
              });
        }
        }
      else{
        StreamBuilder<bool>(
          stream: FlutterBlue.instance.isScanning,
          initialData: false,
          builder: (c, snapshot) {
            if (snapshot.data!) {
              return FloatingActionButton(
                child: Icon(Icons.stop),
                onPressed: () => FlutterBlue.instance.stopScan(),
                backgroundColor: Colors.red,
              );
            } else {
              return FloatingActionButton(
                  child: Icon(Icons.search),
                  onPressed: () async {
                    reload();
                  });
            }
          },
        );
      }
      }
    Widget? discoveryPage(){
      if (useZeroConfDiscovery){
        return ZCDiscoveryBody(notifyParent: reload,discoveredDevicesInfo:_discoveredServices,);
      }
      else{
        return BTDiscoveryBody(notifyParent: reload,);
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('My Devices',
          style: Theme.of(context).textTheme.headline6,),
      ),
      drawer: AppDrawerScreen(),
      floatingActionButton: floatingButton(),
      body: discoveryPage(),
    );
  }
}


