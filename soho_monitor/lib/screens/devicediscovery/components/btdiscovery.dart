// Copyright 2021, mSense Inc
// All rights reserved.

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/screens/dashboard/web-dashboard.dart';
import 'package:soho_monitor/screens/devicediscovery/components/linkeddevice.dart';
import 'package:soho_monitor/screens/devicediscovery/components/scanresult.dart';
import 'package:soho_monitor/screens/wifidiscovery/wifiscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:soho_monitor/models/deviceinfo.dart';
import 'package:soho_monitor/services/ucs/ucs.dart';
import 'dart:convert';

import 'package:soho_monitor/widgets/emaildialog.dart';

class BTDiscoveryBody extends StatefulWidget {
  final Function() notifyParent;
  const BTDiscoveryBody({Key? key, required this.notifyParent})
      : super(key: key);
  @override
  _BTDiscoveryBodyState createState() => _BTDiscoveryBodyState();
}

class _BTDiscoveryBodyState extends State<BTDiscoveryBody> {
  final List <String> _linkedDevices = [];
  final BehaviorSubject<List<DeviceInfo>> _btDevices = BehaviorSubject.seeded([]);
  Stream<List<DeviceInfo>> get btDevices => _btDevices.stream;


  // function to be used by child widgets to trigger update of the main screen
  void loadLinkedDevices()  {
    _linkedDevices.clear();
    _btDevices.add([]);
     List<String>? btDevicesList = AppSettings.instance.linkedDevices;
    if(btDevicesList != null){
      // create Device Info Object List add to bt devices stream to the list
      List<DeviceInfo> devices = [];
      btDevicesList.forEach((btDevice) {

        DeviceInfo device = DeviceInfo.fromJson(jsonDecode(btDevice));
        devices.add(device);
      });
      _btDevices.add(devices);
    }
    //FlutterBlue.instance.stopScan(); // Stop any existing scan before starting a new scan to avoid exceptions and no results
    //FlutterBlue.instance.startScan(timeout: Duration(seconds: 5));

  }

  @override
  void initState()  {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    loadLinkedDevices(); // get linked devices stream
     return SingleChildScrollView(
          child: Column(
            children: <Widget>[
              StreamBuilder<List<DeviceInfo>>(
                stream: btDevices,
                initialData: [],
                builder: (c, snapshot)
                {
                  if (snapshot.data!.isNotEmpty){
                    return Column(
                      children: snapshot.data!
                          .map(
                            (r) {

                          if(r.name.length>0){
                            print("discovered Device: ${r.name}");
                            if (!_linkedDevices.contains(r.btId))
                              _linkedDevices.add(r.btId);
                            // Get Bluetooth Device corresponding to this btID and connect
                            //WiFiService.findBtDevice(r.btId).then((_device) => _device?.connect(autoConnect: false));
                            return LinkedDeviceTile(
                              name: r.name,
                              btId: r.btId,
                              notifyParent: widget.notifyParent,
                              onTap: ()  async{
                                String activationUrl = serverUrl;
                                UCSClient  ucsClient = new UCSClient();
                                String? userId = AppSettings.instance.userId;
                                if (userId != null) {
                                  UserCreationResponse response = await ucsClient.add(userId);
                                  if (response ==
                                      UserCreationResponse.userExists) {
                                    activationUrl =
                                        ucsClient.getActivationURL();
                                  }
                                }
                                Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (context) {
                                return WebDashboard(activationUrl: activationUrl);
                              })).then((value) => null);
                                },
                            );
                          }
                          else {
                            return Container(); // Return Empty Container if it is not a Hawkeye Device
                          }
                        },
                      )
                          .toList(),
                    );
                  } else return Container();
                },
              ),
              FutureBuilder<List<BluetoothDevice>>(
                future: FlutterBlue.instance.connectedDevices,
                builder: (c, AsyncSnapshot<List<BluetoothDevice>> snapshot)
                {
                  if (snapshot.hasData){
                    return Column(
                      children: snapshot.data!
                          .map(
                            (device) {
                          // Add check for HawkEye device later to filter other Bluetooth Devices
                          // if(r.device.name.contains("HawkEye")
                              //print("Connected Device name:" + device.name);
                          //    && device.name.toLowerCase().contains("hawkeye")
                          if(device.name.isNotEmpty   && device.name.toLowerCase().contains("hawk") && !_linkedDevices.contains(device.id.toString())){
                            print("Connected Devices: $device");
                            return ScanResultTile(
                              device: device,
                              onTap: () async {
                                // Get  user email id before  proceeding further
                                if(AppSettings.instance.userId == null)
                                  {
                                    // Add the user
                                   bool ret =  await showEmailDialog(context);
                                   if (ret == false){
                                     Navigator.of(context).pop();
                                   }
                                  }
                                Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (context) {
                                return WifiDiscoveryScreen(btDevice: device);

                              })).then((value) =>  widget.notifyParent()
                              );
                                },
                            );
                          }
                          else {
                            return Container(); // Return Empty Container if it is not a Hawkeye Device
                          }
                        },
                      )
                          .toList(),
                    );
                  } else {
                    return Container();
                  }
                },
              ),
              StreamBuilder<List<ScanResult>>(
                stream: FlutterBlue.instance.scanResults,
                initialData: [],
                builder: (c, snapshot)
                {
                  if (snapshot.data!.isNotEmpty){
                    return Column(
                      children: snapshot.data!
                          .map(
                            (r) {
                          // Add check for HawkEye device later to filter other Bluetooth Devices
                          // if(r.device.name.contains("HawkEye")
                              //&& r.device.name.toLowerCase().contains("hawkeye")
                          print(r.device);
                          if(r.device.name.isNotEmpty  && r.device.name.toLowerCase().contains("hawk") && !_linkedDevices.contains(r.device.id.toString())){

                            return ScanResultTile(
                              device: r.device,
                              onTap: () async {
                                // Get  user email id before  proceeding further
                                if(AppSettings.instance.userId == null)
                                {
                                  // Add the user
                                  bool ret =  await showEmailDialog(context);
                                  if (ret == false){
                                    Navigator.of(context).pop();
                                  }
                                }
                                Navigator.of(context)
                                    .push(MaterialPageRoute(builder: (context) {
                                  return WifiDiscoveryScreen(btDevice: r.device);
                                })).then((value) =>  widget.notifyParent()
                                );
                              },
                            );
                          }
                          else {
                            return Container(); // Return Empty Container if it is not a Hawkeye Device
                          }
                        },
                      )
                          .toList(),
                    );
                  } else {
                    return Container();
                    }
                },
              ),
            ],
          ),
        );
  }
}

