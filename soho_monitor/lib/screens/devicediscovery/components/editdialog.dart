// Copyright 2021, mSense Inc
// All rights reserved.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/deviceinfo.dart';

Future <void> updateLinkedDevice(String btId, String name) async{
  // Add Information to shared prefs for future connection and filtering
  // obtain shared preferences
  // Not the optimal implementation to be optimized later
  AppSettings.instance.setDeviceName(btId, name);
  // update name in btDevices List as well
  // Add if this does not exist
  List <String>? btDevices = AppSettings.instance.linkedDevices;
  int updateIndex = -1;
  int indexCounter = 0;
  String updateDeviceInfo="{}";
  if (btDevices !=null)
    {
      btDevices.forEach((btDevice) {
        DeviceInfo device = DeviceInfo.fromJson(jsonDecode(btDevice));
        if (device.btId == btId)
        {
          DeviceInfo updatedDevice = new DeviceInfo(name: name, btId: btId);
          updateIndex = indexCounter;
          updateDeviceInfo = jsonEncode(updatedDevice);
        }
        indexCounter = indexCounter +1;
      });
      btDevices.removeAt(updateIndex);
      btDevices.add(updateDeviceInfo);
      AppSettings.instance.setLinkedDevices(btDevices);
    }
  }


Future <bool> showEditDialog(BuildContext context, String btId , String currentName) async {
  bool refresh = false;
  final textController = TextEditingController();
  textController.text = currentName;
  refresh = await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Edit Device Name",
          style: Theme.of(context).textTheme.subtitle1,),
        content: TextField(
          controller: textController,
        ),
        actions: <Widget>[
          new TextButton(
            child: new Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          new TextButton(
            child: Text("Save"),
            onPressed: () async {
              await updateLinkedDevice(btId,textController.text);
              Navigator.of(context).pop(true);
            },
          ),
        ],
      );
    },
  );
  return refresh;
}