// Copyright 2021, mSense Inc
// All rights reserved.

import 'dart:async';
import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_mdns_plugin/flutter_mdns_plugin.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/models/constants.dart';
import 'package:soho_monitor/screens/dashboard/web-dashboard.dart';
import 'package:soho_monitor/screens/devicediscovery/components/linkeddevice.dart';
import 'package:soho_monitor/screens/devicediscovery/components/scanresult.dart';
import 'package:soho_monitor/screens/devicediscovery/components/zcdiscoveryresult.dart';
import 'package:soho_monitor/screens/devicediscovery/components/zcpassworddialog.dart';
import 'package:soho_monitor/screens/wifidiscovery/wifiscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:soho_monitor/models/deviceinfo.dart';
import 'package:soho_monitor/services/ucs/ucs.dart';
import 'dart:convert';

import 'package:soho_monitor/widgets/emaildialog.dart';
import 'package:wifi_iot/wifi_iot.dart';

class ZCDiscoveryBody extends StatefulWidget {
  final Function() notifyParent;
  final LinkedHashMap discoveredDevicesInfo;
  const ZCDiscoveryBody({Key? key, required this.notifyParent, required this.discoveredDevicesInfo})
      : super(key: key);
  @override
  _ZCDiscoveryBodyState createState() => _ZCDiscoveryBodyState();
}

class _ZCDiscoveryBodyState extends State<ZCDiscoveryBody> {
  final List <String> _linkedDevices = [];
  bool _isEnabled = false;
  bool _isConnected = false;
  String ? _connectedSSID;
  @override
  void initState()  {
    super.initState();
    WiFiForIoTPlugin.isEnabled().then((val) {
      _isEnabled = val;
    });

    WiFiForIoTPlugin.isConnected().then((val) {
      _isConnected = val;
    });

    WiFiForIoTPlugin.getSSID().then((val) {
      _connectedSSID = val;
    });

  }

  @override
  Widget build(BuildContext context) {
    List<ServiceInfo> _discoveredServicesList = <ServiceInfo>[];
    widget.discoveredDevicesInfo.forEach((name, info) {
      _discoveredServicesList.add(info);
      print("Discovered Devices name in in build ${info.name}");
    });
    print("Discovered Services Info in build ${widget.discoveredDevicesInfo.toString()}");
    if (_discoveredServicesList.length>0)
     return ListView.builder(
             itemCount: _discoveredServicesList.length,
             itemBuilder: (BuildContext context, int index) {
            return ZCResultTile(device:_discoveredServicesList[index],
            onTap: () async{
                  if (_isConnected)
                    {
                      //_connectedSSID = await WiFiForIoTPlugin.getSSID();
                      print("Connected SSID: ${_connectedSSID}");
                      showPasswordDialog(context,_connectedSSID);

                    }
              },);
          })
        ;
    else
      return Container();
  }
}

