// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_mdns_plugin/flutter_mdns_plugin.dart';
import 'package:soho_monitor/models/appsettings.dart';


class ZCResultTile extends StatelessWidget {
  const ZCResultTile({Key? key, required this.device, this.onTap})
      : super(key: key);

  final ServiceInfo device;
  final VoidCallback? onTap;

  Widget _buildTitle(BuildContext context) {
    print("Building Tile ${device.name}");
    String deviceName = AppSettings.instance.getUniqueName(device.name);
    if (device.name.length > 0  ) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 1.0),
              child: ListTile(
                title: Text(
                  deviceName,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
            ),
          ),
        ],
      );

    } else {
      return Text(device.name);
    }
  }

  String getNiceHexArray(List<int> bytes) {
    return '[${bytes.map((i) => i.toRadixString(16).padLeft(2, '0')).join(', ')}]'
        .toUpperCase();
  }

  String getNiceManufacturerData(Map<int, List<int>> data) {
    if (data.isEmpty) {
      return 'N/A';
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add(
          '${id.toRadixString(16).toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  String getNiceServiceData(Map<String, List<int>> data) {
    if (data.isEmpty) {
      return 'N/A';
    }
    List<String> res = [];
    data.forEach((id, bytes) {
      res.add('${id.toUpperCase()}: ${getNiceHexArray(bytes)}');
    });
    return res.join(', ');
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child:Card(

            child:ListTile(
              title: _buildTitle(context),
              /*trailing:  Icon(
          Icons.arrow_forward_ios_sharp,
          size: 35.0,
          //color: Colors.indigo[900],
        )*/
            )
        ));
  }
}
