// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:soho_monitor/screens/dashboard/web-dashboard.dart';
import 'dart:async';
import 'package:soho_monitor/widgets/password.dart';


Future <bool> showPasswordDialog(BuildContext context, String? ssid) async {
  final textController = TextEditingController();
  bool ret =false;
  ret = await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(ssid!,
          style: Theme.of(context).textTheme.subtitle1,),
        content: PasswordTextField(
          controller: textController,
        ),
        actions: <Widget>[
          new TextButton(
            child: new Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          new TextButton(
            child: Text("Configure"),
            onPressed: () async {

              //Navigator.of(context).pop(true);
              Navigator.push(context, MaterialPageRoute(builder: (_) => WebDashboard(activationUrl: "hawkeye.msenseiot.com")));
            },
          ),
        ],
      );
    },
  );
 return ret;
}
