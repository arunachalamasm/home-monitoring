// Copyright 2021, mSense Inc
// All rights reserved.
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:soho_monitor/models/appsettings.dart';
import 'package:soho_monitor/widgets/password.dart';
import 'errordialog.dart';

class Body extends StatefulWidget {
  @override
  _PinChangeState createState() => _PinChangeState();
}
class _PinChangeState extends State<Body> {
  bool _pinSet = false;
  @override
  Widget build(BuildContext context) {
    String? pin = AppSettings.instance.passCode;
    if(_pinSet == false && pin != null ){
      _pinSet = true;
    }
    final textControllerCurrent = TextEditingController();
    textControllerCurrent.text = "";
    final  textControllerNew = TextEditingController();
    textControllerNew.text = "";
    final textControllerConfirm = TextEditingController();
    textControllerConfirm.text = "";
    return  SingleChildScrollView(
          padding:EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if(_pinSet)
              PasswordTextField(
                controller: textControllerCurrent,
                keyboardType: TextInputType.numberWithOptions(signed: false, decimal: true),
                maxLength: 6,
                decorationText: "Current PIN",
              ),
              PasswordTextField(
                controller: textControllerNew,
                keyboardType: TextInputType.numberWithOptions(signed: false, decimal: true),
                maxLength: 6,
                decorationText: "Set PIN",
              ),
              PasswordTextField(
                controller: textControllerConfirm,
                keyboardType: TextInputType.numberWithOptions(signed: false, decimal: true),
                maxLength: 6,
                decorationText: "Confirm PIN",
              ),
              TextButton(
                child: Text("Save"),
                onPressed: () async {
                  String? _pin =  AppSettings.instance.passCode;
                  print(" Current PIN on Pressed: $_pin");
                  if(textControllerNew.text != textControllerConfirm.text){
                    showErrorDialog(context);
                  }
                  else {
                    AppSettings.instance.setPassCode(textControllerNew.text);
                    Navigator.of(context).pop(true);
                  }
                },
              ),
            ],
          ),
    );

  }
        }

