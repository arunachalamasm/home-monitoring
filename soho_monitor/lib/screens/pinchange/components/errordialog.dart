// Copyright 2021, mSense Inc
// All rights reserved.


import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';

Future <void> showErrorDialog(BuildContext context) async {
  await showDialog(
    context: context,
    barrierDismissible: false,  // Modal Dialog
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("PIN Error",
          style: Theme.of(context).textTheme.subtitle1,),
        content: Text("PIN Mismatch. Try Again!!",
          style: Theme.of(context).textTheme.caption,),
        actions: <Widget>[
          new TextButton(
            child: new Text("OK"),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
        ],
      );
    },
  );
}