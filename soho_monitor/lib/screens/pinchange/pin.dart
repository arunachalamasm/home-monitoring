// Copyright 2021, mSense Inc
// All rights reserved.
import 'package:flutter/material.dart';
import 'package:soho_monitor/screens/appdrawer/drawer.dart';
import 'package:soho_monitor/screens/pinchange/components/body.dart';
// Home screen after application launch

class PinConfigurationScreen extends StatefulWidget {
  @override
  _PinConfigurationScreenState createState() => _PinConfigurationScreenState();
}

class _PinConfigurationScreenState extends State<PinConfigurationScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: AppDrawerScreen(),
      appBar: AppBar(
          title: Text("Change PIN ",
            style: Theme.of(context).textTheme.headline6,),
          leading: IconButton(icon: Icon(Icons.arrow_back_rounded),onPressed: (){
            Navigator.pop(context);
          },)
      ),
        body: Body(),
    );
  }
}
