# dashboard

This contains 2 dashbaord:
1. device_provision_dashboard - for add hawkeye device
2. dashboard - for hawkeye device wifi configuration and device monitoring

# rule_chains

This contains 3 rules:
1. add_gateway_attribute - for adding hawkeye device attributes(Device Name and IP Address and Version) after device provision 
2. gateway_device_relation_update - for creating new relation(Manages/Contains) between hawkeye device and hawkeye-node device
3. root_rule_chain -  this is root rule chain for rule engine, with this you are able to filter, enrich and transform incoming messages originated by devices, You are also able to trigger various actions, for example, notifications or communication with external systems.

# wigdet

This contains one wigdet:
1. device_provision - for backend code for device provison dashboard. 